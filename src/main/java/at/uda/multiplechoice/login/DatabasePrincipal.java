package at.uda.multiplechoice.login;

import org.jboss.security.SimplePrincipal;
//this class is required for login, do not delete!
public class DatabasePrincipal extends SimplePrincipal {
	 
	private static final long serialVersionUID = 1L;
     
    //required when creating principal See AbstractServerLoginModule.createIdentity();
    public DatabasePrincipal(String userName) {
        super(userName);
    }
}
