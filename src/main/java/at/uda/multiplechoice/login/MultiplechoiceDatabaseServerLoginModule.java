package at.uda.multiplechoice.login;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.jboss.logging.Logger;
import org.jboss.security.PicketBoxMessages;
import org.jboss.security.auth.spi.DatabaseServerLoginModule;

public class MultiplechoiceDatabaseServerLoginModule extends DatabaseServerLoginModule implements Serializable {

	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(getClass());
	private byte[] salted;
	private byte[] passwordEncrypted;
	

	@Override //indivual login
	protected boolean validatePassword(String enteredPassword, String encrypted) {
		try {
			this.init();
		} catch (LoginException e1) {
			e1.printStackTrace();
		}
		if (enteredPassword == null || encrypted == null) {
			return false;
		}

		try {
			boolean erg= EncryptionBean.authenticate(enteredPassword, passwordEncrypted, salted);
			return erg;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private void init() throws LoginException {//JPA is not available here
		String username = getUsername();
		boolean trace = log.isTraceEnabled();
		ResultSet rs = null;

		Transaction tx = null;
		if (suspendResume) {
			try {
				if (tm == null)
					throw PicketBoxMessages.MESSAGES.invalidNullTransactionManager();
				tx = tm.suspend();
			} catch (SystemException e) {
				throw new RuntimeException(e);
			}
		}
		try {
			InitialContext ctx = new InitialContext();
			DataSource dataSource = (DataSource) ctx.lookup(dsJndiName);
			try (Connection connection = dataSource.getConnection();

					PreparedStatement statement = connection.prepareStatement(
							"SELECT passWordSalt, passWordEncrypted FROM Person WHERE emailAddress=?;")) {

				statement.setString(1, username);
				rs = statement.executeQuery();

				if (rs.next() == false) {
					if (trace) {
						log.trace("Query returned no matches from db");
					}
					throw PicketBoxMessages.MESSAGES.noMatchingUsernameFoundInPrincipals();
				}
				salted = rs.getBytes(1);
				passwordEncrypted = rs.getBytes(2);

			} catch (SQLException ex) {
				LoginException le = new LoginException(PicketBoxMessages.MESSAGES.failedToProcessQueryMessage());
				le.initCause(ex.getCause());
				log.error(ex);
				throw le;
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
					}
				}
			}

		} catch (NamingException ex) {
			LoginException le = new LoginException(
					PicketBoxMessages.MESSAGES.failedToLookupDataSourceMessage(dsJndiName));
			le.initCause(ex);
			log.error(ex);
			throw le;
		} finally {
			if (suspendResume) {
				try {
					tm.resume(tx);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
				if (log.isTraceEnabled())
					log.trace("resumeAnyTransaction");
			}
		}
	}
}
