package at.uda.multiplechoice.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import at.uda.multiplechoice.model.AnswerExam;
import at.uda.multiplechoice.model.AnswerTest;
import at.uda.multiplechoice.model.Classroom;
import at.uda.multiplechoice.model.Exam;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.model.TestDate;
import at.uda.multiplechoice.model.TestEntity;

@PermitAll
@Stateless
public class ExamService {
	@Inject
	private EntityManager entityManager;
	@Inject
	private EntityService entityService;

	public TestDate createRealExam(Classroom classroom, TestEntity test, TestDate testDate) {
		List<Student> students = entityService.getListByQueryParam(Student.class, Student.findStudentsClassroom,
				classroom);
		for (Student student : students) {
			Exam exam = createExam(test, testDate);
			exam = entityManager.find(Exam.class, exam.getId());
			exam.setTestDate(testDate);
			entityManager.merge(exam);
			mergeStudentToExam(exam, student);
		}
		mergeTestDateToClassroom(testDate, classroom);
		return testDate;
	}

	public Exam createSimulation(TestEntity test, TestDate testDate) {
		Exam exam = createExam(test, testDate);
		exam = entityManager.find(Exam.class, exam.getId());
		testDate.addExam(exam);
		testDate.setTest(test);
		exam.setTestDate(testDate);
		entityManager.persist(testDate);	
		return exam;
	}

	private Exam createExam(TestEntity test, TestDate testDate) {
		test = entityManager.find(TestEntity.class, test.getId());
		test.getQuestions().size();
		List<QuestionTest> questions = test.getQuestionsActive();
		Exam exam = new Exam();
		Collections.shuffle(questions);//shuffle questions
		for (QuestionTest questionTest : questions) {
			QuestionExam questionExam = createExamQuestion(questionTest);
			questionTest = entityManager.find(QuestionTest.class, questionTest.getId());
			questionTest.getAnswersTest().size();
			List<AnswerTest> answersTest = questionTest.getAnswersTestActive();
			List<AnswerExam> answersExam = new ArrayList<AnswerExam>();
			Collections.shuffle(answersTest);
			for (AnswerTest answerTest : answersTest) {
				answersExam.add(createAnswerExam(answerTest, questionExam));
			}
			questionExam.setAnswersExam(answersExam);
			exam.getQuestions().add(questionExam);
			questionExam.setExam(exam);
		}
		
		entityManager.persist(exam);
		return exam;
	}

	private void mergeStudentToExam(Exam exam, Student student) {
		student = entityManager.find(Student.class, student.getId());
		student.addExam(exam);
		entityManager.merge(student);
	}

	private void mergeTestDateToClassroom(TestDate testDate, Classroom classroom) {
		classroom = entityManager.find(Classroom.class, classroom.getId());
		classroom.addTestDate(testDate);
		entityManager.merge(classroom);

	}

	private AnswerExam createAnswerExam(AnswerTest answerTest, QuestionExam questionExam) {
		AnswerExam answerExam = new AnswerExam();
		answerExam.setAnswer(answerTest.getAnswer());
		answerExam.setOk(answerTest.isOk());
		answerExam.setQuestionExam(questionExam);
		return answerExam;
	}

	private QuestionExam createExamQuestion(QuestionTest questionTest) {
		QuestionExam questionExam = new QuestionExam();
		questionExam.setOrderNumber(questionTest.getOrderNumber());
		questionExam.setDifficulty(questionTest.getDifficulty());
		questionExam.setText(questionTest.getText());
		questionExam.setTitle(questionTest.getTitle());
		questionExam.setAnswersExam(new ArrayList<AnswerExam>());
		return questionExam;
	}

}
