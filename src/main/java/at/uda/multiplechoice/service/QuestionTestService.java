package at.uda.multiplechoice.service;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.model.Teacher;
import at.uda.multiplechoice.model.TestEntity;

@PermitAll
@Stateless
public class QuestionTestService {
	@Inject
	private EntityManager entityManager;



	public void doSave(QuestionTest question, TestEntity test, Teacher teacher) {
		if(test==null){
			doSaveOrphan(question, teacher);
			return;
		}
		test = entityManager.find(TestEntity.class, test.getId());
		if (question.getId() == 0) {
			teacher = entityManager.find(Teacher.class, teacher.getId());
			teacher.addQuestion(question);
			question.setOrderNumber(test.getQuestions().size() + 1);
			question.setTest(test);
			entityManager.persist(question);
		} else {
			entityManager.merge(question);
		}
	}
	
	public void doSaveOrphan(QuestionTest question, Teacher teacher) {//TEACHER!!
		if (question.getId() == 0) {
			teacher = entityManager.find(Teacher.class, teacher.getId());
			teacher.addQuestion(question);
			entityManager.persist(question);
		} else {
			entityManager.merge(question);
		}
	}

}
