package at.uda.multiplechoice.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import at.uda.multiplechoice.model.AbstractEntity;
@PermitAll
@Stateless
public class EntityService{

	@Inject
	private EntityManager entityManager;

	
	
	
	//2x verwendbar: archive , noarchive mit Odernummer
	//2x higher / lower mit Ordernnumber
	

	public<T extends AbstractEntity,U extends AbstractEntity> void addEntityToParent(T entity,U entityParent){
		entity.setParent(entityParent);
		entityManager.merge(entity);
	}
	
	public<T extends AbstractEntity> void removeEntityFromParent(T entity){
		entity.setParent(null);
		entityManager.merge(entity);
	}
	
	public <T extends AbstractEntity> T openStart(Class<T> clazz, T entity){ 
		entity = entityManager.find(clazz, entity.getId());
		entity.getChildren().size();
		return entity;
	}
	
	
	public <T extends AbstractEntity,U extends AbstractEntity>void save(T entity, U parentEntity) {
		if (entity.getId() == 0) {
			entity.setParent(parentEntity);
			entityManager.persist(entity);
		} else {
			entityManager.merge(entity);
		}
	}
	
	public <T extends AbstractEntity>void remove(Class<T> clazz,T entity) {
			entity=entityManager.find(clazz,entity.getId());
			entity.removeForeignKeys();
			entityManager.remove(entity);
	}
	
	
	
	public <T extends AbstractEntity>void doCancel(Class<T> clazz,T entity, T entityStart) {
		if (entity.getId() == 0) {
			return;
		}
		if (entityStart == null) {
			entityManager.remove(entity);
			return;
		}
		entity = entityManager.find(clazz, entity.getId());
		
		entity.getChildren().size();
		List<? extends AbstractEntity> questions = entity.getChildren();
		List<? extends AbstractEntity> questionsStart = entityStart.getChildren();
		List<Long> startIds=questionsStart.stream().map(q-> q.getId()).collect(Collectors.toList());
		questions.stream().filter(q -> !startIds.contains(q.getId())).forEach(q->{q.removeForeignKeys();entityManager.remove(q);} );
		entityManager.merge(entityStart);
	}
	
	
	public <T extends AbstractEntity>void archive(Class<? extends AbstractEntity> clazz,T entity,List<T> entityList) {
		int index = entityList.indexOf(entity);
		for(int x=index;x<entityList.size();x++){
			T entityHigher=entityList.get(x);
			entityHigher.setOrderNumber(x);
			save(entityHigher);
		}
		archive(entity);
	}
	public <T extends AbstractEntity>void noArchive(Class<? extends AbstractEntity> clazz,T entity,List<T> entityList) {
		int size=entityList.size();
		entity.setOrderNumber(size+1);
		noArchive(entity);
	}
	
	
	
	public  <T extends AbstractEntity>void doLowerOrder(T question,List<T> questionsTest) {
		if (question.getOrderNumber() > 1) {
			int index = questionsTest.indexOf(question);
			T questionLower = questionsTest.get(index - 1);
			long orderNumberLower = questionLower.getOrderNumber();
			long orderNumber = question.getOrderNumber();
			question.setOrderNumber(orderNumberLower);
			questionLower.setOrderNumber(orderNumber);
			entityManager.merge(question);
			entityManager.merge(questionLower);
		}
	}

	public  <T extends AbstractEntity>void doHigherOrder(T question,List<T> questionsTest) {
		if (question.getOrderNumber() < questionsTest.size()) {
			int index = questionsTest.indexOf(question);
			T questionHigher = questionsTest.get(index + 1);
			long orderNumberHigher = questionHigher.getOrderNumber();
			long orderNumber = question.getOrderNumber();
			question.setOrderNumber(orderNumberHigher);
			questionHigher.setOrderNumber(orderNumber);
			entityManager.merge(question);
			entityManager.merge(questionHigher);
		}
	}
	
	public  <T extends AbstractEntity>void save(T entity) {
		if(entity.getId()==0){
			entityManager.persist(entity);
		}else{
			entityManager.merge(entity);
		}
	}

	
	public  <T extends AbstractEntity>void archive(T obj) {
		obj.setArchive(true);
		entityManager.merge(obj);
	}
	public  <T extends AbstractEntity>void noArchive(T obj) {
		obj.setArchive(false);
		entityManager.merge(obj);
	}

	public <T extends AbstractEntity>T find(Class<T> clazz, long id) {
		T managedTest = entityManager.find(clazz, id);
		return managedTest;
	}
	
	public <T> List<T> getList(Class<T> clazz, String queryConstant) {
		TypedQuery<T> query = entityManager.createNamedQuery(queryConstant, clazz);
		return query.getResultList();
	}
	
	public <T,U> T getByQueryParam(Class<T> clazz, String queryConstant, U param) {
		TypedQuery<T> query = entityManager.createNamedQuery(queryConstant, clazz);
		query.setParameter("param", param);
		List<T> results = query.getResultList();
		if (results.size() > 0) {
			return results.get(0);
		} else {
			return null;
		}
	} 
	
	public <T,U> List<T> getListByQueryParam(Class<T> clazz, String queryConstant, U param) {
		TypedQuery<T> query = entityManager.createNamedQuery(queryConstant, clazz);
		query.setParameter("param", param);
		return  query.getResultList();

	}
	

	

}
