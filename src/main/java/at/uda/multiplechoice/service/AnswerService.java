package at.uda.multiplechoice.service;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import at.uda.multiplechoice.data.QuestionTestProducer;
import at.uda.multiplechoice.model.AnswerTest;
import at.uda.multiplechoice.model.QuestionTest;

@PermitAll
@Stateless
public class AnswerService {
	@Inject
	private EntityManager entityManager;
	@Inject
	private QuestionTestProducer testProducer;
	
	public void doSave(AnswerTest answer) {	
		if (answer.getId()==0) {
			QuestionTest question =entityManager.find(QuestionTest.class, testProducer.getSelectedQuestionTest().getId());
			answer.setOrderNumber(question.getAnswersTest().size()+1);
			answer.setQuestionTest(question);
			entityManager.persist(answer);
		} else {
			entityManager.merge(answer);
		}
	}

	
}
