package at.uda.multiplechoice.converter;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.TestDateProducer;
import at.uda.multiplechoice.model.TestDate;

@RequestScoped
@Named
public class TestDateConverter implements Converter {

    @Inject
    TestDateProducer testDateProducer;

    @Override
    public TestDate getAsObject(FacesContext context, UIComponent component, String s) {
    	Long idLong=Long.valueOf(s).longValue(); 
    	testDateProducer.setSelectedTestDate(idLong);  	
        return testDateProducer.getSelectedTestDate();

    }
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	if(value==null){
    		return "";
    	}else{
            return ((TestDate) value).getId() + "";    		
    	}
    }

}
