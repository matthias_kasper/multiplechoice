package at.uda.multiplechoice.converter;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.QuestionTestProducer;
import at.uda.multiplechoice.model.Question;
import at.uda.multiplechoice.model.QuestionTest;

@RequestScoped
@Named
public class QuestionConverter implements Converter {

	@Inject
	QuestionTestProducer questionProducer;

	@Override
	public Question getAsObject(FacesContext context, UIComponent component, String s) {
		QuestionTest question= (QuestionTest) questionProducer.getQuestionByTitle(s);
		questionProducer.setSelectedQuestion(question);
		return question;

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		} else {
			return ((Question) value).getTitle();
		}
	}

}
