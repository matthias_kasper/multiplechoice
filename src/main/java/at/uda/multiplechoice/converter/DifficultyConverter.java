package at.uda.multiplechoice.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import at.uda.multiplechoice.enums.Difficulty;

@FacesConverter(value="difficultyConverter")
public class DifficultyConverter implements Converter {
	public String getAsString(FacesContext context, UIComponent component, Object obj) {
		if (context == null) {
			throw new NullPointerException("context");
		}
		if (component == null) {
			throw new NullPointerException("component");
		}
		if (obj instanceof Difficulty) {
			return ((Difficulty) obj).toString();
		} else {
			return "";
		}
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (context == null) {
			throw new NullPointerException("context");
		}
		if (component == null) {
			throw new NullPointerException("component");
		}

		Difficulty difficulty = null;
		if (value != null && !value.equalsIgnoreCase("") && value.trim().length() > 0) {
			difficulty = Difficulty.get(Integer.valueOf(value));
			if (difficulty == null) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unknown value",
						"Difficulty unknown!");
				throw new ConverterException(message);
			}
		}
		return difficulty;
	}
}
