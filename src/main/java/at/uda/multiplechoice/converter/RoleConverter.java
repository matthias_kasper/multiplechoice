package at.uda.multiplechoice.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import at.uda.multiplechoice.enums.Role;
import at.uda.multiplechoice.enums.Subject;

@FacesConverter(value="roleConverter")//noch nciht getestet
public class RoleConverter implements Converter {
	public String getAsString(FacesContext context, UIComponent component, Object obj) {
		if (context == null) {
			throw new NullPointerException("context");
		}
		if (component == null) {
			throw new NullPointerException("component");
		}
		if (obj instanceof Role) {
			return ((Role) obj).toString();
		} else {
			return "";
		}
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (context == null) {
			throw new NullPointerException("context");
		}
		if (component == null) {
			throw new NullPointerException("component");
		}

		Subject subject = null;
		if (value != null && !value.equalsIgnoreCase("") && value.trim().length() > 0) {
			subject = Subject.get(Integer.valueOf(value));
			if (subject == null) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unknown value",
						"Role unknown!");
				throw new ConverterException(message);
			}
		}
		return subject;
	}
}
