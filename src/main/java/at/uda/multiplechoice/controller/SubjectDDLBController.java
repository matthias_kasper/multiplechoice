package at.uda.multiplechoice.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import at.uda.multiplechoice.enums.Subject;

@ViewScoped
@Named
public class SubjectDDLBController {
	private Subject subject;

	private List<SelectItem> subjects;
	
	@PostConstruct
	public void  init(){
		List<SelectItem> subjects = new ArrayList<SelectItem>();
		for (Subject type : Subject.values()) {
			subjects.add(new SelectItem(type, Subject.getText(type)));
		}
		this.subjects=subjects;
	}

	public List<SelectItem> getSubjects() {
		return subjects;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setSubjects(List<SelectItem> subjects) {
		this.subjects = subjects;
	}

	
}
