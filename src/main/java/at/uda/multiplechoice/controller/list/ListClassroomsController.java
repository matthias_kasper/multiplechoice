package at.uda.multiplechoice.controller.list;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.ClassroomProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.Classroom;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class ListClassroomsController implements Serializable {

	@Inject
	private ClassroomProducer classroomProducer;
	@Inject
	private EntityService entityService;

	private static final long serialVersionUID = 1L;

	public String doArchiveClassroom(Classroom classroom) {
		entityService.archive(classroom);
		return "";
	}
	public String doNoArchiveClassroom(Classroom classroom) {
		entityService.noArchive(classroom);
		return "";
	}


	public String doReadClassroom(Classroom classroom) {
		classroomProducer.prepareEditClassroom(classroom);
		return  Page.EDIT_CLASSROOM.getLabel();
	}

	public List<Classroom> getAllClassrooms() {
		return classroomProducer.getAllClassrooms();
	}
	
	public List<Classroom> getAllArchiveClassrooms() {
		return classroomProducer.getArchiveClassrooms();
	}

	public void doRemoveClassroom(Classroom classroom) {
		entityService.remove(Classroom.class, classroom);
	}
}
