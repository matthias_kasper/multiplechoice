package at.uda.multiplechoice.controller.list;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListStudentsProducer;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class ListStudentsController implements Serializable {

	@Inject
	private ListStudentsProducer listStudentsProducer;
	@Inject 
	private EntityService entityService;

	private static final long serialVersionUID = 1L;
	


	public List<Student> getAllStudentsOrphans() {
		return listStudentsProducer.getStudentsOrphans();
	}
	
	public List<Student> getAllStudentsOrphansArchive() {
		return listStudentsProducer.getStudentsOrphansArchive();
	}

	public List<Student> getStudentsArchive() {
		return listStudentsProducer.getStudentsArchive();
	}
	public List<Student> getStudents() {
		return listStudentsProducer.getStudents();
	}

	public List<Student> getAllStudentsClassroom() {
		return listStudentsProducer.getStudentsClassroom();
	}
	public List<Student> getStudentsClassroomArchive() {
		return listStudentsProducer.getStudentsClassroomArchive();
	}
	
	public String doRemoveStudent(Student student){
		listStudentsProducer.removeStudent(student);
		return "";
	}
	
	public String doArchiveStudent(Student student) {
		entityService.archive(student);
		return "";
	}
	
	

	public String doNoArchiveStudent(Student student) {
		entityService.noArchive(student);
		return "";
	}
}
