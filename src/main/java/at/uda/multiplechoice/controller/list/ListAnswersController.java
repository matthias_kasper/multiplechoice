package at.uda.multiplechoice.controller.list;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListAnswersTestProducer;
import at.uda.multiplechoice.model.AnswerExam;
import at.uda.multiplechoice.model.AnswerTest;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class ListAnswersController implements Serializable {

	@Inject
	private ListAnswersTestProducer listAnswersProducer;
	@Inject
	private EntityService entityService;
	private static final long serialVersionUID = 1L;
	private List<AnswerTest> listAnswersQuestion;

	@PostConstruct
	public void init() {
		listAnswersQuestion = listAnswersProducer.getAnswersTest();
		
	}

	public String doArchiveAnswer(AnswerTest answer) {
		entityService.archive(AnswerTest.class, answer, listAnswersQuestion);
		this.init();
		return "";
	}

	public String remove(AnswerTest answer) {
		listAnswersProducer.remove(answer);
		this.init();
		return "";
	}

	public String doNoArchiveAnswer(AnswerTest answer) {
		entityService.noArchive(AnswerTest.class, answer, listAnswersQuestion);
		this.init();
		return "";
	}

	public String doLowerOrder(AnswerTest answer) {
		entityService.doLowerOrder( answer, listAnswersQuestion);
		this.init();
		return "";
	}

	public List<AnswerTest> getAnswersArchiveQuestion() {
		return listAnswersProducer.getAnswersArchiveQuestion();
	}

	public String doHigherOrder(AnswerTest answer) {
		entityService.doHigherOrder(answer, listAnswersQuestion);
		this.init();
		return "";
	}

	public String doCheckAnswer(AnswerTest answer) {
		listAnswersProducer.doCheckAnswer(answer, true);
		return "";
	}

	public String doUncheckAnswer(AnswerTest answer) {
		listAnswersProducer.doCheckAnswer(answer, false);
		return "";
	}

	public List<AnswerTest> getAllAnswersOrphans() {
		return listAnswersProducer.getAnswersOrphans();
	}

	public List<AnswerTest> getAllAnswersQuestion() {
		return listAnswersQuestion;
	}
	public List<AnswerExam> getAllAnwersExam(){
		return listAnswersProducer.getAnswersExam();
	}

}
