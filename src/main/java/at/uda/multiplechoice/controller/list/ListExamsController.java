package at.uda.multiplechoice.controller.list;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListExamsProducer;
import at.uda.multiplechoice.model.Exam;

@SessionScoped
@Named
public class ListExamsController implements Serializable {


	@Inject
	private ListExamsProducer listExamsProducer;

	private static final long serialVersionUID = 1L;

	public List<Exam> getAllExams() {
		return listExamsProducer.getAllExams();
	}
	public List<Exam> getStudentExams(){
		return listExamsProducer.getStudentExams();
	}
	public List<Exam> getUserStudentExams(){
		return listExamsProducer.getUserExams();
	}

	public List<Exam> getTestDateExams(){
		return listExamsProducer.getTestDateExams();
	}
	public List<Exam> getTestDateExamsArchive(){
		return listExamsProducer.getTestDateExamsArchive();
	}
	



}
