package at.uda.multiplechoice.controller.list;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListQuestionsTestProducer;
import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.model.TestEntity;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class ListQuestionsController implements Serializable {//Nur QuestionTest

	@Inject
	private ListQuestionsTestProducer listQuestionsProducer;
	@Inject
	private EntityService entityService;
	
	

	private static final long serialVersionUID = 1L;
	private List<QuestionTest> listQuestionTest;
	private List<QuestionTest> listQuestionOrphan;
	private List<QuestionTest> listQuestion;
	
	@PostConstruct
	public void init(){
		listQuestionTest=listQuestionsProducer.getQuestionsTest();
		listQuestionOrphan=listQuestionsProducer.getQuestionsOrphans();
		listQuestion=listQuestionsProducer.getQuestions();
		
	}
	
	public String remove(QuestionTest question) {
		entityService.remove(QuestionTest.class,question);
		this.init();
		return "";
	}
	
	public String doArchiveOrphan(QuestionTest question) {
		entityService.archive(QuestionTest.class,question, listQuestionOrphan);
		this.init();
		return "";
	}

	public String doNoArchiveOrphan(QuestionTest question) {
		entityService.noArchive(QuestionTest.class,question, listQuestionOrphan);
		this.init();
		return "";
	}
	
	public String doArchiveQuestionWithTest(QuestionTest question) {
		TestEntity test=entityService.find(TestEntity.class, question.getTest().getId());
		List<QuestionTest> questions=entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findQuestionsTest, test);
		QuestionTest question2=questions.stream().filter( q -> q.getId()==question.getId()).collect(Collectors.toList()).get(0);
		entityService.archive(QuestionTest.class,question2, questions);
		this.init();
		return "";
	}

	public String doNoArchiveQuestionWithTest(QuestionTest question) {
		TestEntity test=entityService.find(TestEntity.class, question.getTest().getId());
		List<QuestionTest> questions=entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findArchiveTest, test);
		QuestionTest question2=questions.stream().filter( q -> q.getId()==question.getId()).collect(Collectors.toList()).get(0);
		List<QuestionTest> questionsActive=entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findQuestionsTest, test);
		entityService.noArchive(QuestionTest.class,question2, questionsActive);
		this.init();
		return "";
	}
	
	

	public String doArchiveQuestion(QuestionTest question) {
		entityService.archive(QuestionTest.class,question, listQuestionTest);
		this.init();
		return "";
	}

	public String doNoArchiveQuestion(QuestionTest question) {
		entityService.noArchive(QuestionTest.class,question, listQuestionTest);
		this.init();
		return "";
	}
	

	public String doLowerOrder(QuestionTest question) {
		entityService.doLowerOrder(question,listQuestionTest);
		this.init();
		return "";
	}

	public String doHigherOrder(QuestionTest question) {
		entityService.doHigherOrder(question,listQuestionTest);
		this.init();
		return "";
	}

	public List<QuestionTest> getAllQuestionsOrphans() {
		return listQuestionOrphan;
	}
	public List<QuestionTest> getAllQuestionsOrphansArchive() {
		return listQuestionsProducer.getQuestionsOrphansArchive();
	}
	
	public List<QuestionTest> getAllQuestions() {
		return listQuestion;
	}

	public List<QuestionTest> getAllQuestionsArchive() {
		return listQuestionsProducer.getQuestionsArchive();
	}
	
	public List<QuestionTest> getQuestionsArchiveTest() {
		return listQuestionsProducer.getQuestionsArchiveTest();
	}

	public List<QuestionTest> getAllQuestionsTest() {
		return listQuestionTest;
	}
	public String doAddQuestionToTest(QuestionTest question){
		listQuestionsProducer.addQuestionToTest(question,listQuestionTest);
		this.init();
		return "";
	}
	
	public String doRemoveQuestion(QuestionTest question){
		listQuestionsProducer.removeQuestion(question);
		this.init();
		return "";
	}
	
	public String doRemoveQuestionFromTest(QuestionTest question){
		listQuestionsProducer.removeQuestionFromTest(question,listQuestionTest);
		this.init();
		return "";
	}


}
