package at.uda.multiplechoice.controller.list;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListTestDatesProducer;
import at.uda.multiplechoice.model.TestDate;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class ListTestDatesController implements Serializable {

	@Inject
	private ListTestDatesProducer listTestDatesProducer;
	@Inject
	private EntityService entityService;

	private static final long serialVersionUID = 1L;
	

	public String archive(TestDate test)  {
		entityService.archive(test);
		return "";
	}

	public String noArchive(TestDate test)  {
		entityService.noArchive(test);
		return "";
	}
	public String remove(TestDate test)  {
		entityService.remove(TestDate.class,test);
		return "";
	}

	public List<TestDate> getAllTestDatesClassroom() {
		return listTestDatesProducer.getTestDatesClassroom();
	}

	public List<TestDate> getAllTestDatesArchive() {
		return listTestDatesProducer.getTestDatesArchive();
	}

	public List<TestDate> getAllTestDate() {
		return listTestDatesProducer.getTestDatesAll();
	}


}