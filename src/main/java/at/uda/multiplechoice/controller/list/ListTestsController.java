package at.uda.multiplechoice.controller.list;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.TestProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.TestEntity;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class ListTestsController implements Serializable {

	@Inject
	private TestProducer testProducer;
	@Inject
	private EntityService entityService;
	private static final long serialVersionUID = 1L;

	private List<TestEntity> listTests;
	
	@PostConstruct
	public void init(){
		listTests=testProducer.getAllTests();
		
	}


	public String doReadTest(TestEntity test) {
		testProducer.prepareEditTest(test);
		return  Page.EDIT_TEST.getLabel();
	}
	
	public String archive(TestEntity test)  {
		entityService.archive(TestEntity.class,test,listTests);
		init();
		return "";
	}

	public String noArchive(TestEntity test)  {
		entityService.noArchive(TestEntity.class,test,listTests);
		init();
		return "";
	}
	

	public String doLowerOrder(TestEntity test)  { 
		entityService.doLowerOrder(test,listTests);
		this.init();
		return "";
	}

	public String doHigherOrder(TestEntity test)  {
		entityService.doHigherOrder(test,listTests);
		this.init();
		return "";
	}
	
	public String remove(TestEntity test)  {
		entityService.remove(TestEntity.class,test);
		return "";
	}

	public List<TestEntity> getAllTests() {
		return listTests;
	}
	
	public List<TestEntity> getAllArchiveTests() {
		return testProducer.getArchiveTests();
	}

	public void doRemoveTest(TestEntity test) {
		entityService.remove(TestEntity.class,test);
	}
}
