package at.uda.multiplechoice.controller.dropdown;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.QuestionTestProducer;
import at.uda.multiplechoice.data.list.ListQuestionsTestProducer;
import at.uda.multiplechoice.model.Question;
import at.uda.multiplechoice.model.QuestionTest;

@SessionScoped
@Named
public class DropdownQuestionController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;;
	@Inject
	ListQuestionsTestProducer listQuestionsProducer;
	@Inject
	QuestionTestProducer questionProducer;

	public Question getSelectedQuestion() {
		return questionProducer.getSelectedQuestionTest();
	}

	public void setSelectedQuestion(QuestionTest question) {
		questionProducer.setSelectedQuestion(question);
	}

	public String doSave() {// Button: entfernen
		return "";
	}

	public List<QuestionTest> getAllQuestions() {
		return listQuestionsProducer.getQuestionsTest();
	}

}
