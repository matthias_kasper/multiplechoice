package at.uda.multiplechoice.controller.dropdown;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.TestDateProducer;
import at.uda.multiplechoice.data.list.ListTestDatesProducer;
import at.uda.multiplechoice.model.TestDate;

@SessionScoped
@Named
public class DropdownTestDateController implements Serializable {
    private static final long serialVersionUID = 2815796004558360299L;
    @Inject
    ListTestDatesProducer listTestDatesProducer;
    @Inject
    TestDateProducer testProducer;
    
    
	public TestDate getSelectedTestDate() {
		return testProducer.getSelectedTestDate();
	}
	
	public void setSelectedTestDate(TestDate test){
		testProducer.setSelectedTestDate(test);
	}


    public String doSave() {//Button: entfernen
        return "";
    }
	
	public List<TestDate> getAllTestDates(){
		return listTestDatesProducer.getTestDatesAll();
	}



}
