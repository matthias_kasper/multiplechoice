package at.uda.multiplechoice.controller.dropdown;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.TestProducer;
import at.uda.multiplechoice.model.TestEntity;

@SessionScoped
@Named
public class DropdownSubjectController implements Serializable {
    private static final long serialVersionUID = 2815796004558360299L;
    @Inject
    TestProducer testProducer;
    
    
	public TestEntity getSelectedTest() {
		return testProducer.getSelectedTest();
	}
	
	public String setSelectedTest(TestEntity test){
		testProducer.setSelectedTest(test);
        return "";
	}


    public String doSave() {//Button: entfernen
        return "";
    }

	public List<TestEntity> getAllTests(){
		return testProducer.getAllTests();
	}



}
