package at.uda.multiplechoice.controller.dropdown;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import at.uda.multiplechoice.enums.Difficulty;

@ViewScoped
@Named
public class DropdownDifficultyController {
	private Difficulty difficulty;

	private List<SelectItem> difficulties;
	
	@PostConstruct
	public void  init(){
		List<SelectItem> difficulties = new ArrayList<SelectItem>();
		for (Difficulty type : Difficulty.values()) {
			difficulties.add(new SelectItem(type, Difficulty.getText(type)));
		}
		this.difficulties=difficulties;
	}

	public List<SelectItem> getDifficulties() {
		return difficulties;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	public void setDifficulties(List<SelectItem> difficulties) {
		this.difficulties = difficulties;
	}

	
}
