package at.uda.multiplechoice.controller.dropdown;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.ClassroomProducer;
import at.uda.multiplechoice.model.Classroom;

@SessionScoped
@Named
public class DropdownClassroomController implements Serializable {
    private static final long serialVersionUID = 2815796004558360299L;
    @Inject
    ClassroomProducer classroomProducer;
    private String classroomId;
    private List<String> classroomIds=new ArrayList<>();
    
    @PostConstruct
    public void init(){
		List<Classroom> classes=classroomProducer.getAllClassrooms();
		classroomIds=classes.stream().map(c -> c.getClassroomId()).collect(Collectors.toList());
    }
    
	public String getClassroomId(){
		return classroomId;
	}
	public void setClassroomId(String str){
		classroomProducer.setSelectedClassroom(str);
		this.classroomId=str;
	}
	

	public List<String> getAllClassroomsIds(){
		return classroomIds;
	}
}
