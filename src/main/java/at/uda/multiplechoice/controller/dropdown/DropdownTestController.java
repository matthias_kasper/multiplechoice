package at.uda.multiplechoice.controller.dropdown;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.TestProducer;
import at.uda.multiplechoice.model.TestEntity;

@SessionScoped
@Named
public class DropdownTestController implements Serializable {//Question Orphan
    private static final long serialVersionUID = 2815796004558360299L;

    @Inject
    private TestProducer testProducer;
    
   
	
    private String title;
    private List<String> titles=new ArrayList<>();
    
    @PostConstruct
    public void init(){
		List<TestEntity> classes=testProducer.getAllTests();
		titles=classes.stream().map(c -> c.getTitle()).collect(Collectors.toList());
    }
    
	public String getTitle(){
		return title;
	}
	public void setTitle(String str){
		testProducer.setSelectedTestByTitle(str);
		this.title=str;
	}
	

	public List<String> getAllTitles(){
		return titles;
	}



}
