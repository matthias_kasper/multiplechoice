package at.uda.multiplechoice.controller.edit;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.controller.list.ListAnswersController;
import at.uda.multiplechoice.data.AnswerTestProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.AnswerTest;

@SessionScoped
@Named
public class EditAnswerController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;
	@Inject
	private AnswerTestProducer answerProducer;
	@Inject
	private ListAnswersController listAnswersController;


	private Page lastPage;
	

	public String doEditAnswer(AnswerTest answer, String lastPage) {
		this.lastPage = Page.valueOf(lastPage);
		answerProducer.prepareEditAnswer(answer);
		listAnswersController.init();
		return Page.EDIT_ANSWER.getLabel();
	}

	public String doAddAnswer(String lastPage) {
		this.lastPage = Page.valueOf(lastPage);;
		answerProducer.prepareAddAnswer();
		listAnswersController.init();
		return Page.EDIT_ANSWER.getLabel();
	}

	public String doAddAnswerToQuestion(AnswerTest answer){
		answerProducer.addAnswerToQuestion(answer);
		return "";
	}
	
	public String doSave() {
		answerProducer.doSave();
		listAnswersController.init();
		return lastPage.getLabel();
	}


	public boolean isEditMode() {
		return !answerProducer.isAddModeAnswerTest();
	}

	public String doCancel() {
		return lastPage.getLabel();

	}




}
