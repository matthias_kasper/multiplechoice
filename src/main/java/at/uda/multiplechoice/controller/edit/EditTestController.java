package at.uda.multiplechoice.controller.edit;



import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.controller.list.ListQuestionsController;
import at.uda.multiplechoice.controller.list.ListTestsController;
import at.uda.multiplechoice.data.TestProducer;
import at.uda.multiplechoice.data.list.ListQuestionsTestProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.TestEntity;
@ManagedBean(name = "editTestController", eager = true)
@SessionScoped
@Named
public class EditTestController implements Serializable {
    private static final long serialVersionUID = 2815796004558360299L;


    @Inject
    private TestProducer testProducer;
    @Inject 
    private ListQuestionsController listQuestionController;
    @Inject 
    private ListTestsController listTestController;
	@Inject 
	private ListQuestionsTestProducer questionsProducer;
    private Page lastPage;
    

	public String doEditTest(TestEntity test,String lastPage) {
		this.lastPage=Page.valueOf(lastPage);
		testProducer.prepareEditTest(test);
		listQuestionController.init();
		return  Page.EDIT_TEST.getLabel();
	}
	
	public String doAddTest(String lastPage) {
		this.lastPage=Page.valueOf(lastPage);
		testProducer.prepareAddTest();	
		listQuestionController.init();
		return  Page.EDIT_TEST.getLabel();
		
	}
	
    public String doSave() {
		int  noOfquestions=questionsProducer.getQuestionsTest().size();
		if(noOfquestions<1 || noOfquestions>100){
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Please enter the correct number of questions (1 - 100).");
			facesContext.addMessage(null, facesMessage);
			return "";
		}
		 testProducer.doSave(listTestController.getAllTests());
		 listTestController.init();
       return lastPage.getLabel();
    }

    public boolean isEditMode(){
    	return ! testProducer.isAddMode();
    }

    
    public String doCancel() {
    	testProducer.doCancel();
       return lastPage.getLabel();
    }


}
