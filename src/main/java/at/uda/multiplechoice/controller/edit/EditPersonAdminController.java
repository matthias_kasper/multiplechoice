package at.uda.multiplechoice.controller.edit;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.PersonProducerAdmin;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.enums.Role;
import at.uda.multiplechoice.model.Admin;
import at.uda.multiplechoice.model.Person;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.model.Teacher;

@SessionScoped
@Named
public class EditPersonAdminController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;

	@Inject
	private PersonProducerAdmin personProducer;
	private Page lastPage;
	
	private List<Country> countries;

	public static class Country {
		private String label;
		private String value;

		public Country(String value, String label) {
			this.label = label;
			this.value = value;
		}

		public String getLabel() {
			return label;
		}

		public String getValue() {
			return value;
		}

	}

	@PostConstruct
	private void init() {
		this.lastPage = Page.ADMIN_START;
	}


	public List<Country> getCountries() {
		return countries;
	}

	public String doLogout() {
		return Page.START.getLabel();
	}

	public String doPersistNewStudent() {
		personProducer.doPersistUser(new Student(),Role.STUDENT);
		return lastPage.getLabel();
	}
	public String doPersistNewTeacher() {
		personProducer.doPersistUser(new Teacher(),Role.TEACHER);
		return lastPage.getLabel();
	}
	public String doPersistNewAdmin() {
		personProducer.doPersistUser(new Admin(),Role.ADMIN);
		return lastPage.getLabel();
	}

	public String doAddPersonAdmin() {		
		this.lastPage =  Page.ADMIN_START;
		personProducer.prepareAddPerson();
		return Page.ADMIN_ADD_PERSON.getLabel();
	}

	public String doEditPasswordAdmin(Person person) {
		this.lastPage = Page.ADMIN_START;
		personProducer.prepareEditPassword(person);

		return Page.ADMIN_EDIT_PASSWORD.getLabel();
	}


	public List<Person> getAllPersons() {
		return personProducer.getAllPersons();
	}

	public String doEditPersonAdmin(Person person) {
		this.lastPage = Page.ADMIN_START;
		personProducer.prepareEditPerson(person);
		return Page.ADMIN_EDIT_PERSON.getLabel();
	}


	public String doRemove(Person person) {
		personProducer.doRemove(person);
		return "";
	}


	public String doSaveWithoutPassword() {
		personProducer.doSaveWithoutPassword();
		return lastPage.getLabel();
	}



	public String doSave() {
		personProducer.doPersistOrSave();
		return lastPage.getLabel();
	}

	public String doCancel() {
		if (lastPage == null) {
			return Page.HOME.getLabel();
		} else {
			return lastPage.getLabel();
		}
	}
}
