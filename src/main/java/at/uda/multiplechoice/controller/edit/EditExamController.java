package at.uda.multiplechoice.controller.edit;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.ExamProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.Exam;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class EditExamController implements Serializable {//Diese Klasse später löschen
	private static final long serialVersionUID = 2815796004558360299L;



	@Inject
	private ExamProducer examProducer;
	@Inject
	private EntityService entityService;

    private Page lastPage;
	
	public String doEditExam(Exam exam,String page) {
		this.lastPage=Page.valueOf(page);	
		examProducer.prepareEditExam(exam);
		return Page.EDIT_EXAM.getLabel();
	}
	public String doReadExam(Exam exam,String page) {
		this.lastPage=Page.valueOf(page);	
		examProducer.setSelectedExam(exam);
		return Page.READ_EXAM.getLabel();
	}

	public List<QuestionExam> getAllQuestionsExam() {
		return examProducer.getQuestionsExam(examProducer.getSelectedExam());
	}
	public String doRemoveExam(Exam exam){
		examProducer.removeExam(exam);
		return "";
	}
	public String archive(Exam exam){
		entityService.archive(exam);
		return "";
	}
	public String noArchive(Exam exam){
		entityService.noArchive(exam);
		return "";
	}
	
	public String doCancel(){
		return lastPage.getLabel();
	}
	

	


}
