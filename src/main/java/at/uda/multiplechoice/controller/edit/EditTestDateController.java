package at.uda.multiplechoice.controller.edit;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.controller.ExamQuestionController;
import at.uda.multiplechoice.data.ClassroomProducer;
import at.uda.multiplechoice.data.ExamProducer;
import at.uda.multiplechoice.data.TestDateProducer;
import at.uda.multiplechoice.data.list.ListQuestionsTestProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.Classroom;
import at.uda.multiplechoice.model.TestDate;

@ManagedBean(name = "editTestDateController", eager = true)
@SessionScoped
@Named
public class EditTestDateController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;

	@Inject
	private TestDateProducer testDateProducer;
	@Inject
	private ExamProducer examProducer;
	@Inject
	private ClassroomProducer classroomProducer;
	@Inject 
	private ExamQuestionController examQuestionController;
	@Inject 
	private ListQuestionsTestProducer questionsProducer;
	private Page lastPage;
	private List<String> classroomIds;
	private String classroomId;

	public String doEditTestDate(TestDate testDate, String lastPage) {
		this.lastPage = Page.valueOf(lastPage);
		testDateProducer.prepareEditTestDate(testDate);
		classroomIds = testDateProducer.getClassroomIds();
		return Page.EDIT_TESTDATE.getLabel();
	}
	public String doReadTestDate(TestDate testDate, String lastPage) {
		this.lastPage = Page.valueOf(lastPage);
		testDateProducer.prepareEditTestDate(testDate);
		classroomIds = testDateProducer.getClassroomIds();
		return Page.READ_TESTDATE.getLabel();
	}


	public String doAddExamClassroom(String lastPage) {
		this.lastPage = Page.valueOf(lastPage);
		this.doSave();
		Classroom classroom = classroomProducer.getClassroomByClassroomId(this.classroomId);
		classroomProducer.setSelectedClassroom(classroom);
		examProducer.prepareAddExamClassroom(testDateProducer.getSelectedTestDate());
		return Page.LIST_TESTDATE.getLabel();
	}

	public String doAddRealExam(String lastPage) {
		
		if(!validate()) return "";		
		this.lastPage = Page.valueOf(lastPage);
		classroomIds = testDateProducer.getClassroomIds();
		testDateProducer.prepareAddTestDate();
		return Page.EDIT_TESTDATE.getLabel();
	}
	
	public String doAddExamSimulation(String lastPage) {
		if(!validate()) return "";	
		
		examProducer.prepareSimulationExam();
		return examQuestionController.startExam();
	}
	
	private boolean validate(){
		int  noOfquestions=questionsProducer.getQuestionsTest().size();
		if(noOfquestions<1 || noOfquestions>100){
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Please enter the correct number of questions (1 - 100).");
			facesContext.addMessage(null, facesMessage);
			return false;
		}
		return true;
	}

	public String doAddTestDateToTest(TestDate testDate) {
		testDateProducer.addTestDateToTest(testDate);
		return "";
	}


	public String doSave() {
		testDateProducer.doSave();
		return lastPage.getLabel();
	}
	
	public String deleteTestExam(){	
		testDateProducer.remove();
		return Page.EDIT_TEST.getLabel();
	}

	public boolean isEditMode() {
		return !testDateProducer.isAddModeTestDate();
	}

	public String doCancel() {
		return lastPage.getLabel();

	}

	public List<String> getClassroomIds() {
		return classroomIds;
	}

	public void setClassroomIds(List<String> classroomIds) {
		this.classroomIds = classroomIds;
	}

	public String getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(String classroomId) {
		this.classroomId = classroomId;
	}

}