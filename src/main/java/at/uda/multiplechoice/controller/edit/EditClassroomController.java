package at.uda.multiplechoice.controller.edit;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.ClassroomProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.Classroom;

@ManagedBean(name = "editClassroomController", eager = true)
@SessionScoped
@Named
public class EditClassroomController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;
    @Inject
    private ClassroomProducer classroomProducer;
    
    private Page lastPage;
    

	public String doEditClassroom(Classroom classroom,String lastPage) {
		this.lastPage=Page.valueOf(lastPage);
		classroomProducer.prepareEditClassroom(classroom);
		return  Page.EDIT_CLASSROOM.getLabel();
		
	}
	
	
	public String doAddClassroom(String lastPage) {
		this.lastPage=Page.valueOf(lastPage);
		classroomProducer.prepareAddClassroom();	
		return  Page.EDIT_CLASSROOM.getLabel();
	}
	
    public String doSave() {
		 classroomProducer.doSave();
       return lastPage.getLabel();
    }

    public boolean isEditMode(){
    	return ! classroomProducer.isAddModeClassroom();
    }
    public boolean hasStudents(){
    	return classroomProducer.getSelectedClassroom().getStudents().size()>0;
    }
    
    public String doCancel() {
    	classroomProducer.doCancel();
       return lastPage.getLabel();
    }

}
