package at.uda.multiplechoice.controller.edit;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.controller.list.ListAnswersController;
import at.uda.multiplechoice.controller.list.ListQuestionsController;
import at.uda.multiplechoice.data.QuestionTestProducer;
import at.uda.multiplechoice.data.list.ListAnswersTestProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.model.QuestionTest;

@SessionScoped
@Named
public class EditQuestionController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;

	@Inject
	private QuestionTestProducer questionProducer;
	@Inject
	private ListAnswersController listAnswersController;
	@Inject 
	private ListQuestionsController listQuestionsController;
	@Inject
	private ListAnswersTestProducer answers;

	private Page lastPage;
	

	public String doEditQuestion(QuestionTest question, String lastPage) {
		
		this.lastPage = Page.valueOf(lastPage);
		questionProducer.prepareEditQuestion(question);
		listAnswersController.init();
		return Page.EDIT_QUESTION.getLabel();
	}

	public String doAddQuestion(String lastPage) {//gleich bei Orphan
		this.lastPage = Page.valueOf(lastPage);
		questionProducer.prepareAddQuestion();
		listAnswersController.init();
		return Page.EDIT_QUESTION.getLabel();
	}
	public String doReadQuestion(QuestionTest question,String lastPage) {
		this.lastPage = Page.valueOf(lastPage);
		questionProducer.setSelectedQuestion(question);
		listAnswersController.init();
		
		return Page.READ_QUESTION.getLabel();
	}
	
	public String doReadQuestionExam(QuestionExam question,String lastPage) {
		this.lastPage = Page.valueOf(lastPage);
		questionProducer.setSelectedQuestion(question);
		listAnswersController.init();
		
		return Page.READ_QUESTION_EXAM.getLabel();
	}
	public String cancelQuestionExam(){
		return lastPage.getLabel();
	}
	
	public boolean isExamQuestion(){
		return questionProducer.isExamQuestion();
	}
	

	
	public String doSave() {
		int  noOfanswers=answers.getAnswersTest().size();
		if(noOfanswers<1 || noOfanswers>10){
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Please enter the correct number of answers (1 - 10).");
			facesContext.addMessage(null, facesMessage);
			return "";
		}
		questionProducer.doSave();
		listQuestionsController.init();
		
		return lastPage.getLabel();
	}

	public String doSaveOrphan() {
		questionProducer.doSaveOrphan();
		listQuestionsController.init();
		return lastPage.getLabel();
	}
	public String doPreview(String lastPage) {
		return Page.PREVIEW_QUESTION.getLabel();
	}
	public String cancelPreview() {
		return 	Page.EDIT_QUESTION.getLabel();
	}


	public boolean isEditMode() {
		return !questionProducer.isAddModeQuestionTest();
	}

	public boolean isOrphan() {
		return lastPage.equals(Page.LIST_QUESTION_ORPHAN);
	}

	public String doCancel() {
		questionProducer.doCancel();
		listQuestionsController.init();		
		return lastPage.getLabel();


	}



}
