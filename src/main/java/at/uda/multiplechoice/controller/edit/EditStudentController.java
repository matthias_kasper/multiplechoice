package at.uda.multiplechoice.controller.edit;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.StudentProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.Student;

@SessionScoped
@Named
public class EditStudentController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;

	@Inject
	private StudentProducer studentProducer;

	private Page lastPage;

	public String doEditStudent(Student question, String lastPage) {

		this.lastPage = Page.valueOf(lastPage);
		studentProducer.prepareEditStudent(question);
		return Page.EDIT_STUDENT.getLabel();
	}

	public String doStartStudent() {
		return Page.STUDENT_START.getLabel();
	}

	public String doAddStudent(String lastPage) {// gleich bei Orphan
		this.lastPage = Page.valueOf(lastPage);
		studentProducer.prepareAddStudent();

		return Page.ADD_STUDENT.getLabel();
	}

	public String doReadStudent(Student student, String lastPage) {
		this.lastPage = Page.valueOf(lastPage);
		studentProducer.setSelectedStudent(student);
		return Page.READ_STUDENT.getLabel();
	}

	public String doAddStudentToClassroom() {
		studentProducer.addStudentToClassroom();
		return lastPage.getLabel();
	}
	public String doEditStudentPassword(Student student, String lastPage){
		this.lastPage = Page.valueOf(lastPage);
		studentProducer.setSelectedStudent(student);
		return Page.EDIT_STUDENT_PASSWORD.getLabel();
	}
	
	
	public String doAddStudentToClassroom(Student student) {
		studentProducer.addStudentToClassroom(student);
		return "";
	}
	public String doRemoveStudentFromClassroom() {
		studentProducer.removeStudentFromClassroom();
		return lastPage.getLabel();
	}
	
	public String doRemoveStudentFromClassroom(Student student) {
		studentProducer.removeStudentFromClassroom(student);
		return "";
	}

	public String doSave() {//ident mit orphan
		studentProducer.doSave();
		return lastPage.getLabel();
	}

	public String doSaveWithoutPassword() {
		studentProducer.doSaveWithoutPassword();
		return lastPage.getLabel();
	}

	public boolean isEditMode() {
		return !studentProducer.isAddModeStudent();
	}

	public boolean isOrphan() {	
		return studentProducer.getSelectedStudent().getClassroom()==null;
	}

	public String doCancel() {
		return lastPage.getLabel();

	}

}
