package at.uda.multiplechoice.controller.edit;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.PersonProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.enums.Role;
import at.uda.multiplechoice.model.Person;

@SessionScoped
@Named
public class EditPersonController implements Serializable {//Only for the Session-User!
	private static final long serialVersionUID = 2815796004558360299L;

	@Inject
	private PersonProducer personProducer;
	private Page lastPage;
	private List<Country> countries;

	public static class Country {
		private String label;
		private String value;

		public Country(String value, String label) {
			this.label = label;
			this.value = value;
		}

		public String getLabel() {
			return label;
		}

		public String getValue() {
			return value;
		}

	}

	@PostConstruct
	private void init() {
		this.lastPage = Page.HOME;
		initCountries();
	}

	private void initCountries() {
		Locale locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
		String[] locales = Locale.getISOCountries();
		countries = new ArrayList<Country>();
		for (String countryCode : locales) {
			Locale obj = new Locale(locale.getLanguage(), countryCode);
			countries.add(new Country(obj.getCountry(), obj.getCountry() + "-" + obj.getDisplayCountry(locale)));
		}
	}

	public List<Country> getCountries() {
		return countries;
	}

	public String doLogout() throws IOException {
	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    ec.invalidateSession();
	    String redirect=ec.getRequestContextPath() + "/public/start.xhtml";
	    ec.redirect(redirect);//without redirect no logout!!
		return Page.HOME.getLabel();
	}
	public String doLogin(){
		return Page.LOGIN.getLabel();
	}
	public String doHome(){
		return Page.HOME.getLabel();
	}

	public String editPersonData() {
		personProducer.getLoggedinPerson();
		return "editPerson.xhtml";
	}

	public String doEditPerson() {// eigene Daten ändern
		Person person = personProducer.prepareEditPerson();
		String page = person.getUserRole().getLabel() + "_START";
		this.lastPage = Page.valueOf(page);
		return Page.EDIT_PERSON.getLabel();
	}

	public String doEditPassword() {
		Person person = personProducer.getLoggedinPerson();
		String page = person.getUserRole().getLabel() + "_START";
		this.lastPage = Page.valueOf(page);
		personProducer.prepareEditPassword();
		return Page.EDIT_PASSWORD.getLabel();
	}

	public String doSaveWithoutPassword() {
		personProducer.doSaveWithoutPassword();
		return lastPage.getLabel();
	}

	public String doPersistNewStudent() throws IOException {
		personProducer.doPersistStudent();
		doLogout();
		return Page.HOME.getLabel();
	}

	public String doSave() {
		personProducer.doPersistOrSave();
		return lastPage.getLabel();
	}

	public String doCancel() {
		if (lastPage == null) {
			return Page.HOME.getLabel();
		} else {
			return lastPage.getLabel();
		}
	}

	public List<String> getAllPersonEmail() {
		return personProducer.getAllPersonEmail();
	}

	public boolean isUserAdmin() {
		Role role = personProducer.getUserRole();
		if (role == null) {
			return false;
		} else {
			return role.equals(Role.ADMIN);
		}
	}

	//
	public boolean isUserTeacher() {
		Role role = personProducer.getUserRole();
		if (role == null) {
			return false;
		} else {
			return role.equals(Role.TEACHER);
		}
	}

	public boolean isUserStudent() {
		Role role = personProducer.getUserRole();
		if (role == null) {
			return false;
		} else {
			return role.equals(Role.STUDENT);
		}
	}

	public String getStartPage() {
		personProducer.init();
		if (isUserAdmin()) {
			return ".." + Page.ADMIN_START.getLabel() + ".xhtml";
		} else if (isUserTeacher()) {
			return ".." + Page.TEACHER_START.getLabel() + ".xhtml";
		} else if(isUserStudent()){
			return ".." + Page.STUDENT_START.getLabel() + ".xhtml";
		}else{
			return ".." + Page.HOME.getLabel() + ".xhtml";
		}
	}

	public boolean isUserHasRole() {
		Role role = personProducer.getUserRole();
		if (role == null) {
			return false;
		} else {
			return true;
		}
	}
}
