package at.uda.multiplechoice.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import at.uda.multiplechoice.enums.Page;

@SessionScoped
@Named
public class NavigationController implements Serializable {

	private static final long serialVersionUID = 1L;
	private Page navigationPage;
	@PostConstruct
	private void init(){
		navigationPage=Page.LIST_TEST;
	}

	public String getNavigationPage() {		
		return navigationPage.getLabel();
	}
	public String navigate(String page){
		navigationPage=Page.valueOf(page.toUpperCase());
		return navigationPage.getLabel();
	}

}