package at.uda.multiplechoice.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.AnswerExamProducer;
import at.uda.multiplechoice.data.ExamProducer;
import at.uda.multiplechoice.data.QuestionExamProducer;
import at.uda.multiplechoice.data.list.ListAnswersExamProducer;
import at.uda.multiplechoice.enums.Page;
import at.uda.multiplechoice.model.AnswerExam;
import at.uda.multiplechoice.model.QuestionExam;

@SessionScoped
@Named
public class ExamQuestionController implements Serializable {
	private static final long serialVersionUID = 2815796004558360299L;

	// questionExam.xhtml
	private int index;

	@Inject
	private QuestionExamProducer questionProducer;
	@Inject
	private ListAnswersExamProducer listAnswersProducer;
	@Inject
	private AnswerExamProducer answerProducer;
	@Inject
	private ExamProducer examProducer;
	@Inject
	private ListAnswersExamProducer answersProducer;
	private int size;

	public String startExam() {
		List<QuestionExam> questions = examProducer.getSelectedExam().getQuestions();		
		this.size=questions.size();
		QuestionExam question = questions.get(0);
		questionProducer.setSelectedQuestionExam(question);
		index = 0;
		return gotoNextQuestion();
	}


	public List<AnswerExam> getAnswersQuestion() {
		return listAnswersProducer.getAnswersQuestion();
	}
	public String gotoPreviousQuestion(){
		List<QuestionExam> questions = examProducer.getSelectedExam().getQuestions();
		this.saveExamQuestion();
		if (index>0) {
			QuestionExam questionNext = questions.get(index-2);
			questionProducer.setSelectedQuestionExam(questionNext);
		} else {
			return Page.EXAM_RESULT.getLabel();
		}
		index--;
		return Page.QUESTION_EXAM.getLabel();
	}

	public String gotoNextQuestion() {
		List<QuestionExam> questions = examProducer.getSelectedExam().getQuestions();
		this.saveExamQuestion();
		if (size > index) {
			QuestionExam questionNext = questions.get(index);
			questionProducer.setSelectedQuestionExam(questionNext);
		} else {
			examProducer.calculateExamResult();
			return Page.EXAM_RESULT.getLabel();
		}
		index++;
		return Page.QUESTION_EXAM.getLabel();
	}



	private void saveExamQuestion() {
		QuestionExam question = questionProducer.getSelectedQuestionExam();
		List<AnswerExam> answers = answersProducer.getAnswersQuestion();
		boolean correct = true;
		for (AnswerExam answer : answers) {
			if (answer.isOk() != answer.isOkUser()) {
				correct = false;
			}
		}
		question.setCorrect(correct);
		question.setAnswersExam(answers);
		examProducer.doSaveExam();
		
	}

	public String doCancel() {
		return Page.EDIT_TEST.getLabel();
	}

	public String doCheckAnswerUser(AnswerExam answer) {
		answerProducer.doCheck(answer, true);
		return "";
	}

	public String doUncheckAnswerUser(AnswerExam answer) {
		answerProducer.doCheck(answer, false);
		return "";
	}

	public int getSize() {
		return size;
	}

}
