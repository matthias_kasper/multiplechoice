package at.uda.multiplechoice.util;

import javax.faces.bean.ManagedBean;

import javax.faces.bean.RequestScoped;

import javax.faces.context.FacesContext;
import javax.inject.Named;

@ManagedBean
@RequestScoped
@Named
public class ErrorHandler {

	public String getStatusCode() {
		String val = String.valueOf((Integer) FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.status_code"));
		return val;
	}
	public String getMessage() {
		String val = (String) FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.message");
		return val;
	}
}