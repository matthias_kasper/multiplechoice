package at.uda.multiplechoice.validator;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@RequestScoped
@FacesValidator("PassGradesValidator")
public class PassGradesValidator implements Validator {
	public PassGradesValidator() {
	}
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, String> themap = ec.getRequestParameterMap();

		String checkGrades=themap.get("theform:checkGrades");
		

		
		if(checkGrades!=null){
			String strpass4=themap.get("input_theform:pass4");
			String strpass3=themap.get("input_theform:pass3");
			String strpass2=themap.get("input_theform:pass2");
			String strpass1=themap.get("input_theform:pass1");
			if(strpass4.equals("")){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 4");
				throw new ValidatorException(msg);
			}else if(strpass3.equals("")){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 3");
				throw new ValidatorException(msg);
			}else if(strpass2.equals("")){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 2");
				throw new ValidatorException(msg);
			}else if(strpass1.equals("")){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 1");
				throw new ValidatorException(msg);
			}
			
			double pass4 = Double.parseDouble(strpass4);
			double pass3 = Double.parseDouble(strpass3);
			double pass2 = Double.parseDouble(strpass2);
			double pass1 = Double.parseDouble(strpass1);
			if(pass3<10.0d || pass3>99.99d){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 3");
				throw new ValidatorException(msg);
			}else if(pass2<10.0d || pass2>99.99d){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 2");
				throw new ValidatorException(msg);
			}else if(pass1<10.0d || pass1>99.99d){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 1");
				throw new ValidatorException(msg);
			}else if(pass4<10.0d || pass4>99.99d){
				FacesMessage msg = new FacesMessage("Please enter the right value for Pass 4");
				throw new ValidatorException(msg);
			}
			
			if(pass4>pass3 || pass3>pass2 || pass2>pass1){
				FacesMessage msg = new FacesMessage("Please enter the right order of values to the pass grades");
				throw new ValidatorException(msg);
			}
		}

	}

}
