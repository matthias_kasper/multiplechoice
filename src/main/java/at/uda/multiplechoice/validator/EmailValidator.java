package at.uda.multiplechoice.validator;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;

import org.jboss.logging.Logger;
import org.jboss.security.PicketBoxMessages;

import at.uda.multiplechoice.util.RegexExpressions;

@RequestScoped
@FacesValidator("EmailValidator")
public class EmailValidator implements Serializable, Validator {
	private static final long serialVersionUID = 1L;

	private static final String EMAIL_PATTERN = RegexExpressions.EMAIL;

	private Pattern pattern;
	private Matcher matcher;
	private Logger log = Logger.getLogger(getClass());
	private String dsJndiName = "java:/jboss/datasources/multiplechoice";
	private List<String> emails = new ArrayList<>();

	public EmailValidator() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		String strValue = value.toString();
		matcher = pattern.matcher(strValue);
		if (!matcher.matches()) {

			FacesMessage msg = new FacesMessage("E-mail validation failed.", "Invalid E-mail format.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);

		} else {
			try {
				init();
			} catch (LoginException e) {
				e.printStackTrace();
			}
			for (String email : emails) {
				if (strValue.equals(email)) {
					FacesMessage msg = new FacesMessage("E-mail validation failed.",
							"Email already in use. Please choose a different email.");
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(msg);
				}
			}

		}

	}

	private void init() throws LoginException {// JPA is not available here
		boolean trace = log.isTraceEnabled();
		ResultSet rs = null;
		try {
			InitialContext ctx = new InitialContext();
			DataSource dataSource = (DataSource) ctx.lookup(dsJndiName);
			try (Connection connection = dataSource.getConnection();

					PreparedStatement statement = connection.prepareStatement("SELECT emailAddress FROM Person;")) {

				rs = statement.executeQuery();

				if (rs.next() == false) {
					if (trace) {
						log.trace("Query returned no matches from db");
					}
					throw PicketBoxMessages.MESSAGES.noMatchingUsernameFoundInPrincipals();
				}
				emails.clear();
				while (rs.next()) {
					emails.add(rs.getString(1));
				}

			} catch (SQLException ex) {
				LoginException le = new LoginException(PicketBoxMessages.MESSAGES.failedToProcessQueryMessage());
				le.initCause(ex.getCause());
				log.error(ex);
				throw le;
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
					}
				}
			}

		} catch (NamingException ex) {
			LoginException le = new LoginException(
					PicketBoxMessages.MESSAGES.failedToLookupDataSourceMessage(dsJndiName));
			le.initCause(ex);
			log.error(ex);
			throw le;
		}
	}
}
