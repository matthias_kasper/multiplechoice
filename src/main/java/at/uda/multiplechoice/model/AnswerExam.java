package at.uda.multiplechoice.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
		@NamedQuery(name = "AnswerExam.findAnswersQuestionExam", query = "SELECT a FROM AnswerExam a  WHERE a.questionExam =:param AND a.archive = FALSE ORDER BY a.orderNumber") })

@DiscriminatorValue(value = "Exam")
@Entity
public class AnswerExam extends Answer {
	public static String findAnswersQuestionExam = "AnswerExam.findAnswersQuestionExam";
	private boolean okUser = false;

	@ManyToOne()
	private QuestionExam questionExam;

	public boolean isOkUser() {
		return okUser;
	}

	public void setOkUser(boolean okUser) {
		this.okUser = okUser;
	}

	public QuestionExam getQuestionExam() {
		return questionExam;
	}

	public void setQuestionExam(QuestionExam questionExam) {
		this.questionExam = questionExam;
	}
}
