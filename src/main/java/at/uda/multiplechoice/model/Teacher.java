package at.uda.multiplechoice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@DiscriminatorValue(value = "TEACHER")
@Entity

public class Teacher extends Person {
	@OneToMany(mappedBy = "teacher", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<TestEntity> tests = new ArrayList<>();
	@OneToMany(mappedBy = "teacher", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	private List<QuestionTest> questionTests = new ArrayList<>();

	public List<TestEntity> getTests() {
		return tests;
	}

	public void setTests(List<TestEntity> tests) {
		this.tests = tests;
	}

	public List<QuestionTest> getQuestionTests() {
		return questionTests;
	}

	public void setQuestionTests(List<QuestionTest> questionTests) {
		this.questionTests = questionTests;
	}

	public void addQuestion(QuestionTest questionTest) {
		this.questionTests.add(questionTest);
		questionTest.setTeacher(this);
	}

}