package at.uda.multiplechoice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@NamedQueries({
		@NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s WHERE s.archive = FALSE ORDER BY s.lastName,s.firstName"),
		@NamedQuery(name = "Student.findOrphans", query = "SELECT s FROM Student s WHERE NOT EXISTS (SELECT 1 FROM Classroom c WHERE c = s.classroom) AND s.archive = :param  ORDER BY s.lastName,s.firstName"),
		@NamedQuery(name = "Student.findStudentClassroom", query = "SELECT s FROM Student s WHERE s.classroom =:param AND s.archive = FALSE  ORDER BY s.lastName,s.firstName"),
		@NamedQuery(name = "Student.findStudentClassroomArchive", query = "SELECT s FROM Student s WHERE s.classroom =:param AND s.archive = TRUE  ORDER BY s.lastName,s.firstName"),
		@NamedQuery(name = "Student.findNoOrphan", query = "SELECT s FROM Student s  WHERE EXISTS (SELECT 1 FROM Classroom c WHERE c = s.classroom) AND s.archive = :param ORDER BY s.lastName,s.firstName") })
@DiscriminatorValue(value = "STUDENT")
@Entity
public class Student extends Person {
	public static final String findOrphans = "Student.findOrphans";
	public static final String findStudentsClassroom = "Student.findStudentClassroom";
	public static final String findNoOrphan = "Student.findNoOrphan";
	public static final String findStudentsClassroomArchive = "Student.findStudentClassroomArchive";

	@ManyToOne()
	private Classroom classroom;

	@OneToMany(mappedBy = "student", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Exam> exams = new ArrayList<>();

	public void addExam(Exam exam) {
		this.exams.add(exam);
		exam.setStudent(this);
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}
	@Override
	 public void removeForeignKeys() {
		this.classroom=null;
	}
	
	@Override
	public List<Exam> getChildren() {
		return exams;
	}
	
	@Override
	public <T extends AbstractEntity> void setParent(T entity) {
		classroom= (Classroom) entity;
	};

}