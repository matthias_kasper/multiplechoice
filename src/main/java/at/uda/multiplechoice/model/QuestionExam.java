package at.uda.multiplechoice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
@NamedQueries({ 
	@NamedQuery(name = "Questions.findQuestionsExam", query = "SELECT q FROM QuestionExam q  WHERE q.exam =:param AND q.archive = FALSE  ORDER BY q.orderNumber"),
})
@DiscriminatorValue(value = "Exam")
@Entity
public class QuestionExam extends Question{	
	public static final String findQuestionsExam="Questions.findQuestionsExam";
	
	@ManyToOne()
	private Exam exam;
	
	@OneToMany(mappedBy="questionExam",cascade = {CascadeType.ALL}, fetch=FetchType.LAZY,orphanRemoval=true)
	private List<AnswerExam> answersExam=new ArrayList<>();
	public Exam getExam() {
		return exam;
	}
	public void setExam(Exam exam) {
		this.exam = exam;
	}
	public List<AnswerExam> getAnswersExam() {
		return answersExam;
	}
	public void setAnswersExam(List<AnswerExam> answersExam) {
		this.answersExam = answersExam;
	}


	
}
