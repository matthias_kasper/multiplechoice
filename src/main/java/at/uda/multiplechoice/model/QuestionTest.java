package at.uda.multiplechoice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@NamedQueries({
		@NamedQuery(name = "Question.findOrphans", query = "SELECT q FROM QuestionTest q  WHERE NOT EXISTS (SELECT 1 FROM Test t WHERE t = q.test)  AND q.archive = :param"),	
	@NamedQuery(name = "Questions.findQuestions", query = "SELECT q FROM QuestionTest q    WHERE EXISTS (SELECT 1 FROM Test t WHERE t = q.test)  AND q.archive = :param  ORDER BY q.title,q.test.title"),
		@NamedQuery(name = "Questions.findArchiveTest", query = "SELECT q FROM QuestionTest q    WHERE q.test =:param AND  q.archive = TRUE  ORDER BY q.orderNumber"),
		@NamedQuery(name = "Questions.findQuestionsTest", query = "SELECT q FROM QuestionTest q  WHERE q.test =:param AND q.archive = FALSE  ORDER BY q.orderNumber") })
@DiscriminatorValue(value = "Test")
@Entity
public class QuestionTest extends Question {
	public static final String findQuestionsTest = "Questions.findQuestionsTest";
	public static final String findOrphans = "Question.findOrphans";
	public static final String findQuestions = "Questions.findQuestions";
	public static final String findArchiveTest = "Questions.findArchiveTest";

	@ManyToOne()
	private TestEntity test;
	@ManyToOne()
	private Teacher teacher;// Orphan Questions to teacher

	@OneToMany(mappedBy = "questionTest", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<AnswerTest> answersTest = new ArrayList<>();

	public TestEntity getTest() {
		return test;
	}

	public void setTest(TestEntity test) {
		this.test = test;
	}

	public List<AnswerTest> getAnswersTest() {
		return answersTest;
	}

	public void setAnswersTest(List<AnswerTest> answersTest) {
		this.answersTest = answersTest;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public List<AnswerTest> getAnswersTestActive() {
		List<AnswerTest> active = new ArrayList<>();
		for (AnswerTest answer : answersTest) {
			if (!answer.isArchive()) {
				active.add(answer);
			}
		}
		return active;
	}
	@Override
	public List<? extends AbstractEntity> getChildren() {
		return answersTest;
	}
	@Override
	public void removeForeignKeys() {
		this.teacher=null;
		this.test=null;
	}
	@Override
	public <T extends AbstractEntity> void setParent(T entity) {
		this.test=(TestEntity) entity;
	
	}
	
}
