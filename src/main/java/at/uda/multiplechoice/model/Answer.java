package at.uda.multiplechoice.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Table(name = "Answer")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
@NamedQueries({ @NamedQuery(name = "Answer.findAll", query = "SELECT a FROM Answer a  ORDER BY a.createDate")})
@Entity
public class Answer extends AbstractEntity{

	@NotNull
	@Size( max=60000, message="{message.char60000}")
	@Column(length = 60000)
	private String answer;
	
	@Size( max=4000, message="{message.char4000}")
	@Column(length = 4000)
	private String explanation;//later: learning mode: answer with explanation for solution.

	private boolean ok = false;
	public static final String findAll = "Answer.findAll";

//	@NotNull
//	@Pattern(regexp=RegexExpressions.NAME, message="{message.title}")
//	@Size(min=3, max=100, message="{message.char100}")
	private String title;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
