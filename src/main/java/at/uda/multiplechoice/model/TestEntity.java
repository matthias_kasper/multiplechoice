package at.uda.multiplechoice.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import at.uda.multiplechoice.enums.Difficulty;
import at.uda.multiplechoice.enums.Subject;

@NamedQueries({
		@NamedQuery(name = "Test.findAll", query = "SELECT t FROM Test t WHERE t.archive=false  ORDER BY t.title"),
		@NamedQuery(name = "Test.findArchive", query = "SELECT t FROM Test t WHERE t.archive=true  ORDER BY t.title"),
		@NamedQuery(name = "Test.findTestByTitle", query = "SELECT t FROM Test t WHERE t.archive=false AND t.title= :param ORDER BY t.createDate"),
		@NamedQuery(name = "Test.findTeacherTests", query = "SELECT t FROM Test t  WHERE t.teacher =:param ORDER BY t.createDate DESC"),
		@NamedQuery(name = "Test.findTeacherTestsActive", query = "SELECT t FROM Test t  WHERE t.teacher =:param AND t.archive=false ORDER BY t.orderNumber"),
		@NamedQuery(name = "Test.findTeacherTestsArchive", query = "SELECT t FROM Test t  WHERE t.teacher =:param AND t.archive=true  ORDER BY t.title") })

@Table(name = "Test")
@Entity(name = "Test")
public class TestEntity extends AbstractEntity{

	public static final String findAll = "Test.findAll";
	public static final String findArchive = "Test.findArchive";
	public static final String findTestByTitle = "Test.findTestByTitle";
	public static final String findTeacherTests = "Test.findTeacherTests";
	public static final String findTeacherTestsActive = "Test.findTeacherTestsActive";
	public static final String findTeacherTestsArchive = "Test.findTeacherTestsArchive";
	@Column(nullable = false)
	private int duration = 45;

	@NotNull
	@Size(min = 3, max = 100, message = "{message.char100}")
	private String title;
	
	@Size( max=60000, message="{message.char60000}")
	@Column(length = 60000)
	private String description;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Subject subject = Subject.INFORMATIC;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Difficulty difficulty = Difficulty.MEDIUM;

	@Column(columnDefinition = "bit(1) default 0", nullable = false)
	private boolean passGrades = false;

	@Column(columnDefinition = "Decimal(10,2) default '90.00'")
	private BigDecimal pass1 = new BigDecimal(90.0);

	@Column(columnDefinition = "Decimal(10,2) default '80.00'")
	private BigDecimal pass2 = new BigDecimal(80.0);

	@Column(columnDefinition = "Decimal(10,2) default '75.00'")
	private BigDecimal pass3 = new BigDecimal(75.0);

	@NotNull
	@DecimalMin("10.00")
	@DecimalMax("99.99")
	@Column(columnDefinition = "Decimal(10,2)  default '60.00'")
	private BigDecimal pass4 = new BigDecimal(60.0);

	@Column(columnDefinition = "bit(1) default 0", nullable = false)
	private boolean shuffle = false;

	@ManyToOne()
	private Teacher teacher;
	@OneToMany(mappedBy = "test", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<QuestionTest> questions = new ArrayList<>();

	@OneToMany(mappedBy = "test", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<TestDate> testDates = new ArrayList<>();

	public void addTestDate(TestDate testDate) {
		this.testDates.add(testDate);
		testDate.setTest(this);
	}

	public void addQuestion(QuestionTest question) {
		this.questions.add(question);
		question.setTest(this);
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public List<QuestionTest> getQuestions() {
		if (questions == null) {
			questions = new ArrayList<>();
		}
		return questions;
	}

	public List<QuestionTest> getQuestionsActive() {
		List<QuestionTest> active = new ArrayList<>();
		for (QuestionTest question : questions) {
			if (!question.isArchive()) {
				active.add(question);
			}
		}
		return active;
	}

	public void setQuestions(List<QuestionTest> questions) {
		this.questions = questions;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	public BigDecimal getPass4() {
		return pass4;
	}

	public void setPass4(BigDecimal passScore) {
		this.pass4 = passScore;
	}

	public boolean isShuffle() {
		return shuffle;
	}

	public void setShuffle(boolean shuffle) {
		this.shuffle = shuffle;
	}

	public boolean isPassGrades() {
		return passGrades;
	}

	public void setPassGrades(boolean passGrades) {
		this.passGrades = passGrades;
	}

	public BigDecimal getPass1() {
		return pass1;
	}

	public void setPass1(BigDecimal pass1) {
		this.pass1 = pass1;
	}

	public BigDecimal getPass2() {
		return pass2;
	}

	public void setPass2(BigDecimal pass2) {
		this.pass2 = pass2;
	}

	public BigDecimal getPass3() {
		return pass3;
	}

	public void setPass3(BigDecimal pass3) {
		this.pass3 = pass3;
	}

	public List<TestDate> getTestDates() {
		return testDates;
	}

	public void setTestDates(List<TestDate> testDates) {
		this.testDates = testDates;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	@Override
	public List<? extends AbstractEntity> getChildren() {
		return questions;
	}

	@Override
	public void removeForeignKeys() {
		teacher = null;
	}

	@Override
	public <T extends AbstractEntity> void setParent(T entity) {
		teacher = (Teacher) entity;
	}

}
