package at.uda.multiplechoice.model;
//Neu Zwischen Test und Exam
//Exam für einen Student

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
@NamedQueries({
	@NamedQuery(name="TestDate.findAll", query="SELECT t FROM TestDate t WHERE t.archive=false  ORDER BY t.testDate DESC"),
	@NamedQuery(name="TestDate.findArchive", query="SELECT td FROM TestDate td JOIN td.test t WHERE td.archive=true  ORDER BY td.testDate DESC"),
	@NamedQuery(name = "TestDate.findTestDatesClassroom", query = "SELECT t FROM TestDate t JOIN t.classroom c WHERE c.id =:param AND t.archive = FALSE "),
	@NamedQuery(name = "TestDate.findTestDatesTest", query = "SELECT td FROM TestDate td JOIN td.test t WHERE t.id =:param AND td.archive = FALSE  ORDER BY td.createDate DESC"),
	@NamedQuery(name = "TestDate.findTestDatesTeacherAll", query = "SELECT td FROM TestDate td WHERE td.test.teacher.id =:param AND td.archive = FALSE  ORDER BY td.createDate DESC"),
	@NamedQuery(name = "TestDate.findTestDatesTeacherArchive", query = "SELECT td FROM TestDate td WHERE td.test.teacher.id =:param AND td.archive = TRUE  ORDER BY td.createDate DESC"),
})
@Entity
public class TestDate extends AbstractEntity {
	
	

	@ManyToOne()
	private TestEntity test;
	@ManyToOne()
	private Classroom classroom;	
	private Date testDate;
	
	private boolean simulation=false;

	public static final String findAll="TestDate.findAll";
	public static final String findArchive="TestDate.findArchive";
	public static final String findTestDatesClassroom="TestDate.findTestDatesClassroom";
	public static final String findTestDatesTest="TestDate.findTestDatesTest";
	public static final String findTestDatesTeacherAll="TestDate.findTestDatesTeacherAll";
	public static final String findTestDatesTeacherArchive="TestDate.findTestDatesTeacherArchive";
	
	@OneToMany(mappedBy="testDate",cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
	private List<Exam> exams=new ArrayList<>();
	private boolean finished=false;
	
	public void addExam(Exam exam){
		exam.setTestDate(this);
		this.exams.add(exam);
	}

	public TestEntity getTest() {
		return test;
	}

	public void setTest(TestEntity test) {
		this.test = test;
	}

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}


	public Date getTestDate() {
		return testDate;
	}

	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}



	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public boolean isSimulation() {
		return simulation;
	}

	public void setSimulation(boolean simulation) {
		this.simulation = simulation;
	}
	@Override
	public void removeForeignKeys() {
		this.test=null;
		this.classroom=null;
	}
	@Override
	public <T extends AbstractEntity> void setParent(T entity) {
		test = (TestEntity) entity;
	};

}
