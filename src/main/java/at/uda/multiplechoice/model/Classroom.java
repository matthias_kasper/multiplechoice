package at.uda.multiplechoice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import at.uda.multiplechoice.util.RegexExpressions;

@NamedQueries({
	@NamedQuery(name = "Classroom.findAllClassroomIds", query = "SELECT c.classroomId FROM Classroom c WHERE c.archive=false ORDER BY c.classroomId"),
		@NamedQuery(name = "Classroom.findAll", query = "SELECT c FROM Classroom c WHERE c.archive=false ORDER BY c.classroomId"),
		@NamedQuery(name = "Classroom.findArchive", query = "SELECT c FROM Classroom c WHERE c.archive=true ORDER BY c.classroomId"),
		@NamedQuery(name="Classroom.findClassroomByClassroomId", query="SELECT c FROM Classroom c WHERE c.archive=false AND c.classroomId= :param ORDER BY c.createDate")
		})
@Entity
public class Classroom extends AbstractEntity{
	public static String findAll = "Classroom.findAll";
	public static String findAllClassroomIds = "Classroom.findAllClassroomIds";
	public static String findArchive = "Classroom.findArchive";
	public static String findClassroomByClassroomId = "Classroom.findClassroomByClassroomId";
	
	@NotNull
	@Pattern(regexp=RegexExpressions.NAME, message="{message.classroomid}")
	@Size(min=3, max=10, message="{message.char100}")
	private String classroomId;

	@OneToMany(mappedBy = "classroom", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<Student> students=new ArrayList<>();
	
	

	@OneToMany(mappedBy = "classroom", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<TestDate> testDates=new ArrayList<>();

	
    public void addStudent(Student student) {
        student.setClassroom(this);
        this.students.add(student);
    }
    public void addTestDate(TestDate testDate){
    	testDate.setClassroom(this);
    	this.testDates.add(testDate);
    }
    


	public String getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(String classroomId) {
		this.classroomId = classroomId;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public List<TestDate> getTestDates() {
		return testDates;
	}

	public void setTestDates(List<TestDate> testDates) {
		this.testDates = testDates;
	}
	@Override
	public List<Student> getChildren(){
		return students;
	}

}