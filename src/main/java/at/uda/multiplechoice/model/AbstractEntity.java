package at.uda.multiplechoice.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import at.uda.multiplechoice.listener.EntityListener;

@MappedSuperclass
@EntityListeners({EntityListener.class})
public abstract class AbstractEntity {
	@GeneratedValue
	@Id
	private long id;

	private boolean archive = false;


	private long orderNumber;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate = new Date();

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModified = new Date() ;

	
	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public long getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(long orderNumber) {
		this.orderNumber = orderNumber;
	}
	public  List<? extends AbstractEntity> getChildren(){
		return null;
	}
	public void removeForeignKeys(){	
	}
	
	public  <T extends AbstractEntity>void setParent(T entity){
	}
}
