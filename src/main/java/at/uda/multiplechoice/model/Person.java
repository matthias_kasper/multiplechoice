package at.uda.multiplechoice.model;

import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import at.uda.multiplechoice.util.RegexExpressions;
import at.uda.multiplechoice.enums.Role;
@Table(name = "Person")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
@Entity
@NamedQueries({
    @NamedQuery(name = Person.findByEmail, query = "SELECT p FROM Person p WHERE p.emailAddress = :param"),
    @NamedQuery(name = Person.findAll, query = "SELECT p FROM Person p ORDER BY p.userRole, p.lastName, p.firstName"),
    @NamedQuery(name = Person.findAllEmail, query = "SELECT p.emailAddress FROM Person p ORDER BY p.userRole, p.lastName, p.firstName")
})
public class Person extends AbstractEntity{
	
	public static final String findByEmail="Person.findByEmail";
	public static final String findAll="Person.findAll";
	public static final String findAllEmail="Person.findAllEmail";
	
	@NotNull
	@Pattern(regexp=RegexExpressions.NAME, message="{message.firstname}")
	@Size(min=3, max=100, message="{message.char100}")
	private String firstName;
	
	@NotNull
	@Pattern(regexp=RegexExpressions.NAME, message="{message.lastname}")
	@Size(min=3, max=100, message="{message.char100}")
	private String lastName;
	
	@NotNull
	@Pattern(regexp=RegexExpressions.NAME, message="{message.userid}")
	@Size(min=3, max=100, message="{message.char100}")
	private String userID;
	
	private String passWord;
	
	
//	@NotNull
//	@Pattern(regexp=RegexExpressions.EMAIL, message="{message.emailaddress}")
//	@Size(min=3, max=100, message="{message.char100}")
	private String emailAddress;//=username, validation!")
	
	private String passWordCompare; //transient!
	@Enumerated(EnumType.STRING)
	private Role userRole;
	
	
	private String street;
	
	private String town;
	

	private String country;
	

	private String zipCode;
	

	private String streetnumber;

	
	private Date birthDate;

	
	@Lob
	private byte[]  passWordEncrypted;
	@Lob
	private byte[]  passWordSalt;
	

	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Role getUserRole() {
		return userRole;
	}
	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassWordCompare() {
		return passWordCompare;
	}
	public void setPassWordCompare(String passWordCompare) {
		this.passWordCompare = passWordCompare;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getStreetnumber() {
		return streetnumber;
	}
	public void setStreetnumber(String streetnumber) {
		this.streetnumber = streetnumber;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public byte[] getPassWordEncrypted() {
		return passWordEncrypted;
	}
	public void setPassWordEncrypted(byte[] passWordEncrypted) {
		this.passWordEncrypted = passWordEncrypted;
	}
	public byte[] getPassWordSalt() {
		return passWordSalt;
	}
	public void setPassWordSalt(byte[] passWordSalt) {
		this.passWordSalt = passWordSalt;
	}

}
