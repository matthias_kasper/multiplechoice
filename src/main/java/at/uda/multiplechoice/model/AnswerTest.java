package at.uda.multiplechoice.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
@NamedQueries({
	@NamedQuery(name = "AnswerTest.findOrphans", query = "SELECT a FROM AnswerTest a WHERE NOT EXISTS (SELECT 1 FROM Question t WHERE t = a.questionTest) AND a.archive = FALSE ORDER BY a.title" ),
	@NamedQuery(name = "AnswerTest.findArchive", query = "SELECT a FROM AnswerTest a  WHERE a.archive = TRUE ORDER BY a.title"),
	@NamedQuery(name = "AnswerTest.findArchiveAnswerQuestion", query = "SELECT a FROM AnswerTest a  WHERE a.questionTest=:param AND a.archive = TRUE ORDER BY a.title"),
	@NamedQuery(name = "AnswerTest.findAnswersQuestionTest", query = "SELECT a FROM AnswerTest a WHERE a.questionTest =:param AND a.archive = FALSE  ORDER BY a.orderNumber")})
@DiscriminatorValue(value = "Test")
@Entity
public class AnswerTest extends Answer{
	
	public static final String findOrphans="AnswerTest.findOrphans";
	public static final String findArchive="AnswerTest.findArchive";
	public static final String findAnswersQuestionTest="AnswerTest.findAnswersQuestionTest";
	public static final String findArchiveAnswerQuestion="AnswerTest.findArchiveAnswerQuestion";
	@ManyToOne()
	private QuestionTest questionTest;
	public QuestionTest getQuestionTest() {
		return questionTest;
	}
	public void setQuestionTest(QuestionTest questionTest) {
		this.questionTest = questionTest;
	}
	@Override
	public void removeForeignKeys() {
		questionTest=null;
	}
	
}
