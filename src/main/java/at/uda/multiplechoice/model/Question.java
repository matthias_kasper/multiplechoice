package at.uda.multiplechoice.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import at.uda.multiplechoice.enums.Difficulty;
import at.uda.multiplechoice.util.RegexExpressions;
@Table(name = "Question")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
@NamedQueries({ @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q ORDER BY q.text"),
@NamedQuery(name="Question.findQuestionByTitle", query="SELECT q FROM Question q WHERE q.archive=false AND q.title= :param ORDER BY q.createDate")})
@Entity
public class Question extends AbstractEntity{
	
	public static final String findAll="Question.findAll";
	public static final String findQuestionByTitle="Question.findQuestionByTitle";
	@Column(nullable=false)
	
	@NotNull
	@Pattern(regexp=RegexExpressions.NAME, message="{message.title}")
	@Size(min=3, max=100, message="{message.char100}")
	private String title;
	
	@NotNull
	@Size( max=60000, message="{message.char60000}")
	@Column(length = 60000)
	private String text;
	
	@Column(nullable=false)
	private Difficulty difficulty=Difficulty.MEDIUM;
	@Column(nullable=false)
	private boolean correct=false;

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Difficulty getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}
	public boolean isCorrect() {
		return correct;
	}
	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	
}
