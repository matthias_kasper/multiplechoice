package at.uda.multiplechoice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@NamedQueries({
		@NamedQuery(name = "Exam.findAll", query = "SELECT e FROM Exam e WHERE e.archive = FALSE ORDER BY e.createDate"),
		@NamedQuery(name = "Exam.findExamsTestDate", query = "SELECT e FROM Exam e  WHERE e.testDate =:param AND e.archive = FALSE  ORDER BY e.createDate"),
		@NamedQuery(name = "Exam.findExamsTestDateArchive", query = "SELECT e FROM Exam e  WHERE e.testDate =:param AND e.archive = TRUE  ORDER BY e.createDate"),
		@NamedQuery(name = "Exam.findStudentExams", query = "SELECT e FROM Exam e  WHERE e.student =:param AND e.archive = FALSE   ORDER BY e.createDate") })

@Entity
public class Exam extends AbstractEntity {

	public static String findAll = "Exam.findAll";
	public static final String findStudentExams = "Exam.findStudentExams";
	public static final String findExamsTestDate = "Exam.findExamsTestDate";
	public static final String findExamsTestDateArchive = "Exam.findExamsTestDateArchive";
	private int duration;
	private Double passPercent;
	private int passGrade;
	private boolean pass=false;
	private String studentId;
	private boolean finished=false;
	
	@ManyToOne()
	private TestDate testDate;
	@ManyToOne()
	private Student student;

	@OneToMany(mappedBy = "exam", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY,orphanRemoval=true)
	private List<QuestionExam> questions=new ArrayList<>();

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public List<QuestionExam> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionExam> questions) {
		this.questions = questions;
	}

	public TestDate getTestDate() {
		return testDate;
	}

	public void setTestDate(TestDate testDate2) {
		this.testDate = testDate2;
	}

	public static String getFindAll() {
		return findAll;
	}

	public static void setFindAll(String findAll) {
		Exam.findAll = findAll;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Double getPassPercent() {
		return passPercent;
	}

	public void setPassPercent(Double passPercent) {
		this.passPercent = passPercent;
	}

	public int getPassGrade() {
		return passGrade;
	}

	public void setPassGrade(int passGrade) {
		this.passGrade = passGrade;
	}

	public boolean isPass() {
		return pass;
	}

	public void setPass(boolean pass) {
		this.pass = pass;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	@Override
	public void removeForeignKeys() {
		student=null;
		testDate=null;
	}

}
