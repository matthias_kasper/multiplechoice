package at.uda.multiplechoice.listener;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import at.uda.multiplechoice.model.AbstractEntity;

public class EntityListener {
	  @PrePersist
	  @PreUpdate
	  public void prePersist(AbstractEntity entity) {
	   entity.setLastModified(new Date());
	  }
}
