package at.uda.multiplechoice.enums;

public enum Subject {
    INFORMATIC("Informatic"),
    MATHEMATIC("Mathematic"),
	ENGLISH("English");
	
	
    private final String label;

    private Subject(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
    public static Subject get(Integer ordinal) {
    	return Subject.values()[ordinal];
    }

    public static Integer get(Subject subject) {
    	return subject.ordinal();
    }

    public static String getText(Subject subject) {
    	return subject.getLabel();
    }
     @Override
    public String toString(){
    	return this.getLabel();
    }
}
