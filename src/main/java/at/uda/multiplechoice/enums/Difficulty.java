package at.uda.multiplechoice.enums;

public enum Difficulty {
    EASY("Easy"),
    MEDIUM("Medium"),
    DIFFICULT("Difficult"),
    VERY_DIFFICULT("Very Difficult");

    private final String label;

    private Difficulty(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
    public static Difficulty get(Integer ordinal) {
    	return Difficulty.values()[ordinal];
    }

    public static Integer get(Difficulty difficulty) {
    	return difficulty.ordinal();
    }

    public static String getText(Difficulty difficulty) {
    	return difficulty.getLabel();

    }
     @Override
    public String toString(){
    	return this.getLabel();
    }

}
