package at.uda.multiplechoice.enums;

public enum Role {
    STUDENT("STUDENT"),
    TEACHER("TEACHER"),
	ADMIN("ADMIN");
	public static final String _ADMIN="ADMIN";
	public static final String _TEACHER="TEACHER";
	public static final String _CUSTOMER="CUSTOMER";
	
    private final String label;

    private Role(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
    public static Role get(Integer ordinal) {
    	return Role.values()[ordinal];
    }

    public static Integer get(Role subject) {
    	return subject.ordinal();
    }

    public static String getText(Role subject) {
    	return subject.getLabel();
    }
     @Override
    public String toString(){
    	return this.getLabel();
    }
}
