package at.uda.multiplechoice.enums;



public enum Page {
	EDIT_TEST("/teacher/test/editTest"),
	READ_TEST("/teacher/test/readTest"),
	LIST_TEST("/teacher/test/listTest"),
	LIST_TEST_ARCHIVE("/teacher/test/listTestArchive"),

	EDIT_QUESTION("/teacher/question/editQuestion"),	
	READ_QUESTION("/teacher/question/readQuestion"),
	LIST_QUESTION_ORPHAN("/teacher/question/listQuestionsOrphans"),
	LIST_QUESTION_TEST("/teacher/question/listQuestionsTest"),
	LIST_QUESTION("/teacher/question/listQuestions"),

	EDIT_ANSWER("/teacher/answer/editAnswer"),	

	LIST_EXAM("/teacher/exam/listExam"),
	EDIT_EXAM("/exam/editExam"),
	QUESTION_EXAM("/exam/questionExam"),	    
	EXAM_RESULT("/exam/examResult"),

	LIST_CLASSROOM("/teacher/classroom/listClassroom"),
	LIST_CLASSROOM_ARCHIVE("/teacher/classroom/listClassroomArchive"),
	EDIT_CLASSROOM("/teacher/classroom/editClassroom"),
	READ_CLASSROOM("/teacher/classroom/readClassroom"),//n. v. 
	ADD_STUDENT("/teacher/student/addStudent"),
	EDIT_STUDENT("/teacher/student/editStudent"),
	EDIT_STUDENT_PASSWORD("/teacher/student/editStudentPassword"),
	READ_STUDENT("/teacher/student/readStudent"),//n. v. 
	LIST_STUDENT_ORPHAN("/teacher/student/listStudentOrphan"),
	LIST_STUDENT("/teacher/student/listStudent"),


	LIST_TESTDATE_ARCHIVE("/teacher/testdate/listTestDateArchive"),
	LIST_TESTDATE("/teacher/testdate/listTestDate"),
	EDIT_TESTDATE("/teacher/testdate/editTestDate"),
	LIST_TESTDATE_TEST("/teacher/testdate/listTestDateTest"),
	LIST_EXAM_TESTDATE("/teacher/testdate/listExamsTestDate"),
	LIST_TESTDATE_CLASSROOM("/teacher/testdate/listTestDateClassroom"),
	
	HOME("/public/start"),
	STUDENT_START("/student/start"),
	TEACHER_START("/teacher/start"),
	ADMIN_START("/admin/start"),
	ADMIN_EDIT_PERSON("/admin/editPerson"),
	ADMIN_ADD_PERSON("/admin/newPersonAdmin"),
	ADMIN_EDIT_PASSWORD("/admin/editPassword"),
	EDIT_PERSON("/user/editPerson"),
	EDIT_PASSWORD("/user/editPassword"),
	EDIT_BOOK("editBook"),
	INVOICE("invoice"),
	START("start"),
	ADD_PERSON("/public/newPerson"),
	LOGIN("/user/start"), 
	PREVIEW_QUESTION("/teacher/question/readQuestionPreview"), 
	READ_EXAM("/teacher/exam/readExam"), 
	READ_QUESTION_EXAM("/teacher/exam/readQuestionExam"), 
	READ_TESTDATE("/teacher/testdate/readTestDate");
	
    private final String label;

    private Page(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
    public static Page get(Integer ordinal) {
    	return Page.values()[ordinal];
    }

    public static Integer get(Page page) {
    	return page.ordinal();
    }

    public static String getText(Page page) {
    	return page.getLabel();

    }
     @Override
    public String toString(){
    	return this.getLabel();
    }
}
