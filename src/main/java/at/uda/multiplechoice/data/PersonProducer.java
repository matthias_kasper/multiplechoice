package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.ejb.Remove;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.enums.Role;
import at.uda.multiplechoice.login.EncryptionBean;
import at.uda.multiplechoice.model.Person;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.service.EntityService;
import io.undertow.server.session.Session;

@SessionScoped
@Stateful
@PermitAll
public class PersonProducer implements Serializable {
	private static final long serialVersionUID = -1828660647917534556L;

	@Resource
	private SessionContext sessionContext;
	@Resource
	private Session session;
	private Person person;
	@Inject
	private EntityService entityService;

	@Remove
	public void remove() {

	}

	@PostConstruct
	public void init() {

		if (this.person == null) {
			this.person = getLoggedinPerson();
			if (this.person == null) {
				this.person = new Person();
			}
		}
	}

	@Produces
	@Named
	public Person getSelectedPerson() {
		if (person == null) {
			init();
		}
		return person;
	}

	public void setSelectedPerson(Person person) {
		this.person = person;
	}

	@Produces
	@Named
	public boolean isAddModePerson() {
		return person.getId() == 0;
	}

	public void prepareEditPassword() {
		this.person = getLoggedinPerson();
		person.setPassWord("");
		person.setPassWordCompare("");

	}

	public Person prepareEditPerson() {
		this.person = getLoggedinPerson();
		;
		return person;
	}

	public Role getUserRole() {
		if (getLoggedinPerson() == null) {
			return null;
		} else {
			return person.getUserRole();
		}
	}

	@PreDestroy
	public void logout() {
		this.person = null;
	}

	public Person getLoggedinPerson() {

		if (person == null || person.getUserRole() == null) {
			if (sessionContext == null) {
				return null;
			} else if (sessionContext.getCallerPrincipal() == null) {
				return null;
			} else {
				String email = sessionContext.getCallerPrincipal().getName();
				this.person = entityService.getByQueryParam(Person.class, Person.findByEmail, email);
				return person;
			}
		} else {
			return person;
		}

	}

	public Person prepareAddStudent() {
		this.person = new Student();
		person.setCountry("AT");
		return person;
	}

	public void doSaveWithoutPassword() {
		entityService.save(person);
	}

	public void doPersistOrSave() {
		setPassword(person);
		entityService.save(person);

	}

	private void setPassword(Person person) {
		try {
			person.setPassWordSalt(EncryptionBean.generateSalt());
			person.setPassWordEncrypted(
					EncryptionBean.getEncryptedPassword(person.getPassWord(), person.getPassWordSalt()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}

	public void doCancel() {
	}

	public Person getPersonById(long id) {
		return entityService.find(Person.class, id);
	}

	public List<String> getAllPersonEmail() {
		return entityService.getList(String.class, Person.findAllEmail);
	}

	public void doPersistStudent() {
		Student student = new Student();
		student.setUserRole(Role.STUDENT);
		student.setFirstName(person.getFirstName());
		student.setLastName(person.getLastName());
		student.setEmailAddress(person.getEmailAddress());
		student.setPassWord(person.getPassWord());// Nur für Tests später
													// entfernen
		setPassword(student);
		entityService.save(student);

	}

}
