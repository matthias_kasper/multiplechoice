package at.uda.multiplechoice.data;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class QuestionExamProducer implements Serializable {
	private static final long serialVersionUID = -1828660647917534556L;

	private QuestionExam question;

	// Page questionExam
	@Inject
	private EntityService entityService;

	@Produces
	@Named
	public QuestionExam getSelectedQuestionExam() {
		return question;
	}

	public void setSelectedQuestionExam(QuestionExam question) {
		this.question = question;
	}

	public void setSelectedQuestionExam(long id) {
		this.question = entityService.find(QuestionExam.class, id);
	}

	public void doSave() {
		entityService.save( question);

	}

	@Produces
	@Named
	public boolean isAddModeQuestion() {
		return question.getId() == 0;
	}

	public void prepareAddQuestion() {// auch für Orphan
		QuestionExam question = new QuestionExam();
		this.question = question;
	}

	public void prepareEditQuestion(QuestionExam question) {// auch für Orphan
		this.question = question;
	}

}
