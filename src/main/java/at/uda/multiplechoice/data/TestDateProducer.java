package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.model.Classroom;
import at.uda.multiplechoice.model.TestDate;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Named
public class TestDateProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	private TestDate testDate;

	@Inject
	private TestProducer testProducer;
	
	@Inject
	private EntityService entityService;

	@Produces
	@Named
	public TestDate getSelectedTestDate() {
		return testDate;
	}

	public void setSelectedTestDate(TestDate testDate) {
		this.testDate = testDate;
	}

	public void setSelectedTestDate(long id) {
		this.testDate = entityService.find(TestDate.class, id);
	}

	@Produces
	@Named
	public boolean isAddModeTestDate() {
		return testDate.getId()==0;
	}


	public List<String> getClassroomIds() {
		return entityService.getList(String.class, Classroom.findAllClassroomIds);
	}

	public TestDate prepareAddTestDate() {
		this.testDate=new TestDate();
		testDate.setTestDate(new Date());
		return testDate;
	}

	public TestDate getTestDate() {
		return this.testDate;
	}

	public void prepareEditTestDate(TestDate testDate) {
		this.testDate = testDate;
	}

	public void doSave() {
		entityService.save(testDate,testProducer.getSelectedTest());
	}

	public void addTestDateToTest(TestDate testDate) {
		entityService.addEntityToParent(testDate,testProducer.getSelectedTest());
	}
	public void remove(TestDate testDate) {
		entityService.remove(TestDate.class,testDate);
	}
	public void remove() {
		entityService.remove(TestDate.class,testDate);
	}

}