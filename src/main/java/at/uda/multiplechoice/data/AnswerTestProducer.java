package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.model.AnswerTest;
import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
public class AnswerTestProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	private AnswerTest answerTest;

	@Inject
	private QuestionTestProducer questionProducer;
	@Inject
	private EntityService entityService;

	@Produces
	@Named
	public AnswerTest getSelectedAnswerTest() {
		return answerTest;
	}

	public void setSelectedAnswerTest(AnswerTest answer) {
		this.answerTest = answer;
	}

	@Produces
	@Named
	public boolean isAddModeAnswerTest() {
		return answerTest.getId() == 0;
	}

	public void prepareAddAnswer() {
		if (questionProducer.isAddModeQuestionTest()) {
			questionProducer.doSaveForAnswers();
		}
		AnswerTest answer = new AnswerTest();
		this.answerTest = answer;
	}

	public void prepareEditAnswer(AnswerTest answer) {
		this.answerTest = answer;
	}

	public void doSave() {
		if (this.isAddModeAnswerTest()) {
			this.addAnswerToQuestion(answerTest);
		} else {
			entityService.save( answerTest);
		}
	}

	public void addAnswerToQuestion(AnswerTest answer) {// Auch nach Add Answer
		QuestionTest question = (QuestionTest) questionProducer.getSelectedQuestionTest();
		List<AnswerTest> answersTest = entityService.getListByQueryParam(AnswerTest.class,
				AnswerTest.findAnswersQuestionTest, question);
		answersTest.add(answer);
		answer.setOrderNumber(answersTest.size());

		answer.setQuestionTest(question);
		question.setAnswersTest(answersTest);
		entityService.save( question);
	}

	public void doCheckAnswer(AnswerTest answer, boolean ok) {
		this.answerTest = answer;
		answer.setOk(ok);
		entityService.save(answer);

	}



}
