package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListExamsProducer;
import at.uda.multiplechoice.data.list.ListStudentsProducer;
import at.uda.multiplechoice.model.Classroom;
import at.uda.multiplechoice.model.Exam;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.model.TestDate;
import at.uda.multiplechoice.model.TestEntity;
import at.uda.multiplechoice.service.EntityService;
import at.uda.multiplechoice.service.ExamService;

@SessionScoped
public class ExamProducer implements Serializable {
	private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	ClassroomProducer classroomProducer;
	@Inject
	ListStudentsProducer studentsProducer;
	@Inject
	ListExamsProducer listExamsProducer;
	@Inject
	TestProducer testProducer;
	@Inject
	TestDateProducer testDateProducer;
	@Inject
	EntityService entityService;
	@Inject
	ExamService examService;

	private Exam exam;

	@Produces
	@Named
	public Exam getSelectedExam() {
		return exam;
	}

	public void doSaveExam() {
			entityService.save( exam);

	}

	public void refreshSelectedExam() {
		this.exam = entityService.find(Exam.class, exam.getId());
	}

	public void setSelectedExam(Exam exam) {
		this.exam = exam;
	}

	public void removeExam(Exam exam) {
		entityService.remove(Exam.class,exam);
	}
	
	public void prepareAddExamClassroom(TestDate testDate) {
		Classroom classroom=classroomProducer.getSelectedClassroom();
		TestEntity test=testProducer.getSelectedTest();
		 examService.createRealExam(classroom,test,testDate);
	}

	public void calculateExamResult() {
		refreshSelectedExam();
		TestEntity test = exam.getTestDate().getTest();
		List<QuestionExam> questions = getQuestionsExam(exam);

		int size = questions.size();
		int passedQuestions = 0;
		for (QuestionExam question : questions) {
			if (question.isCorrect()) {
				passedQuestions++;
			}
		}

		double result = (passedQuestions / size) * 100.0d;

		exam.setPassPercent(result);
		if (test.isPassGrades()) {
			exam.setPass(true);
			if (result >= test.getPass1().doubleValue()) {
				exam.setPassGrade(1);
			} else if (result >= test.getPass2().doubleValue()) {
				exam.setPassGrade(2);
			} else if (result >= test.getPass3().doubleValue()) {
				exam.setPassGrade(3);
			} else if (result >= test.getPass4().doubleValue()) {
				exam.setPassGrade(4);
			} else {
				exam.setPass(false);
				exam.setPassGrade(5);
			}

		} else {
			if (result > test.getPass4().doubleValue()) {
				exam.setPass(true);
			} else {
				exam.setPass(false);
			}
		}
		exam.setFinished(true);
		doSaveExam();
	}



	public void prepareSimulationExam() {
		TestDate testDate=testDateProducer.prepareAddTestDate();
		testDate.setSimulation(true);
		TestEntity test=testProducer.getSelectedTest();
		this.exam = examService.createSimulation(test,testDate);
	}

	public List<QuestionExam> getQuestionsExam(Exam exam) {
		return entityService.getListByQueryParam(QuestionExam.class, QuestionExam.findQuestionsExam, exam);
	}

	public void prepareEditExam(Exam exam) {
		List<QuestionExam> questions = entityService.getListByQueryParam(QuestionExam.class,
				QuestionExam.findQuestionsExam, exam);
		exam.setQuestions(questions);
		this.exam = exam;
	}

	public void setSelectedExam(Long id) {
		this.exam = entityService.find(Exam.class, id);

	}

}
