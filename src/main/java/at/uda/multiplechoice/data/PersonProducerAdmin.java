package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.ejb.Remove;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.enums.Role;
import at.uda.multiplechoice.login.EncryptionBean;
import at.uda.multiplechoice.model.Person;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
@Stateful
@PermitAll
public class PersonProducerAdmin implements Serializable {

	static final long serialVersionUID = 1L;
	@Resource
	private SessionContext sessionContext;
	private Person person;
	@Inject
	private EntityService entityService;

	@Remove
	public void remove() {

	}

	@PostConstruct
	public void init() {

	}

	@Produces
	@Named
	public Person getSelectedPersonAdmin() {
		if (person == null) {
			init();
		}
		return person;
	}

	public void setSelectedPersonAdmin(Person person) {
		this.person = person;
	}

	public void prepareEditPassword(Person person) {
		this.person = person;
		person.setPassWord("");
		person.setPassWordCompare("");
	}

	public void doRemove(Person person) {
		this.person = person;
		entityService.remove(Person.class, person);
	}

	public void prepareEditPerson(Person person) {
		this.person = person;

	}

	public Person prepareAddPerson() {
		this.person = new Person();
		return person;
	}

	public void doSaveWithoutPassword() {
		entityService.save( person);
	}

	public void doPersistOrSave() {
		setPassword(person);
		entityService.save( person);
	}

	private void setPassword(Person person) {
		try {
			person.setPassWordSalt(EncryptionBean.generateSalt());
			person.setPassWordEncrypted(
					EncryptionBean.getEncryptedPassword(person.getPassWord(), person.getPassWordSalt()));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}


	public <T extends Person>void doPersistUser(T user,Role role) {
		user.setUserRole(Role.TEACHER);
		user.setFirstName(person.getFirstName());
		user.setLastName(person.getLastName());
		user.setEmailAddress(person.getEmailAddress());
//		user.setPassWord(person.getPassWord());//Testmode
		setPassword(user);
		entityService.save(user);
	}
	

	public void doCancel() {
	}

	public Person getPersonById(long id) {
		return entityService.find(Person.class, id);
	}

	public List<Person> getAllPersons() {
		return entityService.getList(Person.class, Person.findAll);
	}

}
