package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListQuestionsTestProducer;
import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.model.Teacher;
import at.uda.multiplechoice.model.TestEntity;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
public class TestProducer implements Serializable {// Remove-Flag entfernt

	private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	private EntityService entityService;
	@Inject
	private PersonProducer personProducer;
	
	@Inject
	private ListQuestionsTestProducer listProducer;

	private TestEntity test;
	private TestEntity testStart = null;
	



	@Produces
	@Named
	public List<TestEntity> getArchiveTests() {
		
		Teacher teacher = (Teacher) personProducer.getLoggedinPerson();
		return entityService.getListByQueryParam(TestEntity.class, TestEntity.findTeacherTestsArchive, teacher);
	}

	@Produces
	@Named
	public List<TestEntity> getAllTests() {
		Teacher teacher = (Teacher) personProducer.getLoggedinPerson();
		return entityService.getListByQueryParam(TestEntity.class, TestEntity.findTeacherTestsActive, teacher);
	}

	@Produces
	@Named
	public TestEntity getSelectedTest() {
		return test;
	}

	public void setSelectedTest(TestEntity test) {
		this.test = test;
	}

	public void setSelectedTestByTitle(String title) {
		this.test = entityService.getByQueryParam(TestEntity.class, TestEntity.findTestByTitle, title);
	}

	@Produces
	@Named
	public boolean isAddMode() {
		return test.getId() == 0;
	}

	public void prepareAddTest() {
		this.test = new TestEntity();
		test.setQuestions(new ArrayList<QuestionTest>());
	}

	public void refreshSelectedTest() {
		if (test != null) {
			this.test = entityService.find(TestEntity.class, test.getId());
		}
	}

	public void prepareEditTest(TestEntity test) {
		this.testStart = entityService.openStart(TestEntity.class,test);
		this.test = test;

	}

	public void doSaveForQuestion() {
		entityService.save(test, personProducer.getLoggedinPerson());
	}

	public void doSave(List<TestEntity> listTests) {
		List<QuestionTest> questions = listProducer.getQuestionsArchiveTest();
		questions.addAll(listProducer.getQuestionsTest());
		int size=listTests.size();
		test.setOrderNumber(size+1);
		test.setQuestions(questions);
		entityService.save(test, personProducer.getLoggedinPerson());
	}

	public void doCancel() {
		entityService.doCancel(TestEntity.class,test, testStart);

	}

	public TestEntity getTestById(long id) {
		return entityService.find(TestEntity.class, id);
	}

	public TestEntity getTestByTitle(String title) {
		return entityService.getByQueryParam(TestEntity.class, TestEntity.findTestByTitle, title);
	}

}
