package at.uda.multiplechoice.data.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import at.uda.multiplechoice.data.TestProducer;
import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.model.TestEntity;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
public class ListQuestionsTestProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	private TestProducer testProducer;
	@Inject
	private EntityService entityService;

	public List<QuestionTest> getQuestionsOrphans() {
		return entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findOrphans, false);
	}

	public List<QuestionTest> getQuestions() {
		return entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findQuestions, false);
	}

	public List<QuestionTest> getQuestionsArchive() {
		return entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findQuestions, true);
	}

	public List<QuestionTest> getQuestionsArchiveTest() {
		TestEntity test = testProducer.getSelectedTest();
		if (test == null || test.getId() == 0) {
			return new ArrayList<>();
		} else {
			return entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findArchiveTest, test);

		}
	}

	public List<QuestionTest> getQuestionsTest() {
		TestEntity test = testProducer.getSelectedTest();
		if (test == null || test.getId() == 0) {
			return new ArrayList<>();
		} else {
			return entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findQuestionsTest, test);
		}
	}

	public void addQuestionToTest(QuestionTest question, List<QuestionTest> questionsTest) {
		int size = questionsTest.size();
		question.setOrderNumber(size + 1);
		TestEntity test = testProducer.getSelectedTest();
		entityService.addEntityToParent(question, test);

	}

	public void removeQuestion(QuestionTest question) {
		entityService.remove(QuestionTest.class, question);
	}

	public void removeQuestionFromTest(QuestionTest question, List<QuestionTest> questionsTest) {
		int index = questionsTest.indexOf(question);
		for (int x = index; x < questionsTest.size(); x++) {
			QuestionTest questionHigher = questionsTest.get(x);
			questionHigher.setOrderNumber(x);
			entityService.save(questionHigher);
		}
		entityService.removeEntityFromParent(question);
	}

	public List<QuestionTest> getQuestionsOrphansArchive() {
		return entityService.getListByQueryParam(QuestionTest.class, QuestionTest.findOrphans, true);
	}

}
