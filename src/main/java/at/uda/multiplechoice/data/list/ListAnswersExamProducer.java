package at.uda.multiplechoice.data.list;

import java.io.Serializable;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;

import at.uda.multiplechoice.data.QuestionExamProducer;
import at.uda.multiplechoice.model.AnswerExam;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.service.EntityService;

@Stateless
@PermitAll
public class ListAnswersExamProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	private QuestionExamProducer questionProducer;
	@Inject
	private EntityService entityService;


	public List<AnswerExam> getAnswersQuestion() {
		QuestionExam question = questionProducer.getSelectedQuestionExam();
		return entityService.getListByQueryParam(AnswerExam.class, AnswerExam.findAnswersQuestionExam, question);
	}
}
