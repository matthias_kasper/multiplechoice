package at.uda.multiplechoice.data.list;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.PersonProducer;
import at.uda.multiplechoice.data.StudentProducer;
import at.uda.multiplechoice.data.TestDateProducer;
import at.uda.multiplechoice.model.Exam;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.model.TestDate;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
public class ListExamsProducer implements Serializable {

    private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	private EntityService entityService;
	@Inject
	private StudentProducer studentProducer;
	@Inject
	private TestDateProducer testDateProducer;
	@Inject
	private PersonProducer personProducer;	




	
	@Produces
	@Named
	public List<Exam> getAllExams() {
		return entityService.getList(Exam.class,Exam.findAll);
	}

	public List<Exam> getStudentExams() {
		Student student=studentProducer.getSelectedStudent();
		return this.getStudentExams(student);
	}
	public List<Exam> getUserExams() {
		Student student=(Student) personProducer.getLoggedinPerson();
		return this.getStudentExams(student);
	}


	public List<Exam> getStudentExams(Student student) {
		return entityService.getListByQueryParam(Exam.class,Exam.findStudentExams, student);
	}



	public List<Exam> getTestDateExams() {
		TestDate testDate=testDateProducer.getSelectedTestDate();
		return entityService.getListByQueryParam(Exam.class,Exam.findExamsTestDate, testDate);
	}

	public List<Exam> getTestDateExamsArchive() {
		TestDate testDate=testDateProducer.getSelectedTestDate();
		return entityService.getListByQueryParam(Exam.class,Exam.findExamsTestDateArchive, testDate);
	}
	
}
