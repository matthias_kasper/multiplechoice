package at.uda.multiplechoice.data.list;

import java.io.Serializable;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;

import at.uda.multiplechoice.data.ClassroomProducer;
import at.uda.multiplechoice.data.PersonProducer;
import at.uda.multiplechoice.model.Teacher;
import at.uda.multiplechoice.model.TestDate;
import at.uda.multiplechoice.service.EntityService;

@Stateless
@PermitAll
public class ListTestDatesProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;


	@Inject
	private EntityService entityService;
	@Inject
	private ClassroomProducer classroomProducer;
	@Inject
	private PersonProducer personProducer;
	

	public List<TestDate> getTestDatesArchive() {
		Teacher teacher=(Teacher) personProducer.getLoggedinPerson();
		return entityService.getListByQueryParam(TestDate.class, TestDate.findTestDatesTeacherArchive,teacher.getId());
	}

	public List<TestDate> getTestDatesAll() {
		Teacher teacher=(Teacher) personProducer.getLoggedinPerson();
		return entityService.getListByQueryParam(TestDate.class, TestDate.findTestDatesTeacherAll,teacher.getId());
	}
	
	

	public List<TestDate> getTestDatesClassroom() {
		long classroomId=classroomProducer.getSelectedClassroom().getId();
		return entityService.getListByQueryParam(TestDate.class, TestDate.findTestDatesClassroom, classroomId);
	}


	public void doArchive(TestDate testDate, boolean archive) {
		testDate.setArchive(archive);
		entityService.save( testDate);
	}

}