package at.uda.multiplechoice.data.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;

import at.uda.multiplechoice.data.QuestionTestProducer;
import at.uda.multiplechoice.model.Answer;
import at.uda.multiplechoice.model.AnswerExam;
import at.uda.multiplechoice.model.AnswerTest;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.service.EntityService;

@Stateless
@PermitAll
public class ListAnswersTestProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	private QuestionTestProducer questionProducer;
	@Inject
	private EntityService entityService;

	public List<AnswerTest> getAnswersOrphans() {
		return entityService.getList(AnswerTest.class, AnswerTest.findOrphans);
	}

	public List<AnswerTest> getAnswersArchive() {
		return entityService.getList(AnswerTest.class, AnswerTest.findArchive);
	}

	public List<AnswerTest> getAnswersTest() {
		if(!questionProducer.isExamQuestion()){
		QuestionTest question = (QuestionTest) questionProducer.getSelectedQuestionTest();
		if (question==null || question.getId() == 0) {
			return new ArrayList<>();
		} else {
			return entityService.getListByQueryParam(AnswerTest.class, AnswerTest.findAnswersQuestionTest, question);
		}
		}else{
			return new ArrayList<>();
		}
	}

	public void doCheckAnswer(Answer answer, boolean ok) {
		answer.setOk(ok);
		entityService.save(answer);

	}

	public void remove(AnswerTest answer) {
		entityService.remove(AnswerTest.class,answer);
	}

	public List<AnswerTest> getAnswersArchiveQuestion() {
		QuestionTest question = (QuestionTest) questionProducer.getSelectedQuestionTest();
		if (question==null || question.getId() == 0) {
			return new ArrayList<>();
		} else {
			return entityService.getListByQueryParam(AnswerTest.class, AnswerTest.findArchiveAnswerQuestion, question);
			
		}
	}

	public List<AnswerExam> getAnswersExam() {
		QuestionExam question= questionProducer.getSelectedExamQuestion();
		return entityService.getListByQueryParam(AnswerExam.class, AnswerExam.findAnswersQuestionExam, question);
	}

}
