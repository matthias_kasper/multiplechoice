package at.uda.multiplechoice.data.list;

import java.io.Serializable;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;

import at.uda.multiplechoice.data.ClassroomProducer;
import at.uda.multiplechoice.model.Classroom;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.service.EntityService;

@Stateless
@PermitAll
public class ListStudentsProducer implements Serializable {
	private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	private ClassroomProducer classroomProducer;
	@Inject
	private EntityService entityService;


	public List<Student> getStudentsOrphans() {
		return entityService.getListByQueryParam(Student.class,Student.findOrphans,false);
	}
	public List<Student> getStudentsOrphansArchive() {
		return entityService.getListByQueryParam(Student.class,Student.findOrphans,true);
	}
	
	public List<Student> getStudents(){
		return entityService.getListByQueryParam(Student.class,Student.findNoOrphan,false);
	}

	public List<Student> getStudentsArchive() {
		return entityService.getListByQueryParam(Student.class,Student.findNoOrphan,true);
	}

	public List<Student> getStudentsClassroom() {
		Classroom classroom = classroomProducer.getSelectedClassroom();
		return entityService.getListByQueryParam(Student.class,Student.findStudentsClassroom, classroom);
	}
	public List<Student> getStudentsClassroomArchive() {
		Classroom classroom = classroomProducer.getSelectedClassroom();
		return entityService.getListByQueryParam(Student.class,Student.findStudentsClassroomArchive, classroom);
	}

	public void removeStudent(Student student) {	
		entityService.remove(Student.class,student);
		
	}





}
