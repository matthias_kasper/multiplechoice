package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.data.list.ListStudentsProducer;
import at.uda.multiplechoice.model.Classroom;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
public class ClassroomProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	@Inject
	private EntityService entityService;
	@Inject
	private ListStudentsProducer studentsProducer;


	private Classroom classroom;
	private Classroom classroomStart = null;


	@Produces
	@Named
	public List<Classroom> getArchiveClassrooms() {
		return entityService.getList(Classroom.class, Classroom.findArchive);
	}

	@Produces
	@Named
	public List<Classroom> getAllClassrooms() {
		return  entityService.getList(Classroom.class, Classroom.findAll);
	}

	@Produces
	@Named
	public Classroom getSelectedClassroom() {
		return classroom;
	}
	public void setSelectedClassroom(String classroomId) {	
		this.classroom = entityService.getByQueryParam(Classroom.class,Classroom.findClassroomByClassroomId, classroomId);
	}
	
	public void setSelectedClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	public void refreshSelectedClassroom(){
		this.classroom=entityService.find(Classroom.class, classroom.getId());
	}

	@Produces
	@Named
	public boolean isAddModeClassroom() {
		return classroom.getId()==0;
	}

	public void prepareAddClassroom() {
		this.classroom = new Classroom();
	}

	public void prepareEditClassroom(Classroom classroom) {
		this.classroomStart = entityService.openStart(Classroom.class,classroom);
		this.classroom = classroom;

	}

	public void doSave() {
		List<Student> students = studentsProducer.getStudentsClassroom();
		students.addAll(studentsProducer.getStudentsClassroomArchive());
		classroom.setStudents(students);
		entityService.save(classroom);
	}
	public void doSaveForStudents(){
		entityService.save(classroom);
	}

	public void setSelectedClassroom(long id) {
		this.classroom = entityService.find(Classroom.class, id);
	}
	
	public void doCancel() {
		entityService.doCancel(Classroom.class,classroom, classroomStart);
	}

	public Classroom getClassroomById(long id) {
		return entityService.find(Classroom.class, id);
	}

	public Classroom getClassroomByClassroomId(String s) {
		return entityService.getByQueryParam(Classroom.class, Classroom.findClassroomByClassroomId, s);
	}



}
