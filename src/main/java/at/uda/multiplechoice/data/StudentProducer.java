package at.uda.multiplechoice.data;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.enums.Role;
import at.uda.multiplechoice.login.EncryptionBean;
import at.uda.multiplechoice.model.Student;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
public class StudentProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	private Student student;
	private Student studentStart = null;

	@Inject
	private ClassroomProducer classroomProducer;
	@Inject
	private EntityService entityService;

	@Produces
	@Named
	public Student getSelectedStudent() {
		return student;
	}

	public void setSelectedStudent(Student student) {
		this.student = student;
	}

	public void setSelectedStudent(long id) {
		this.student = entityService.find(Student.class, id);
	}

	@Produces
	@Named
	public boolean isAddModeStudent() {
		return student.getId() == 0;
	}

	public void prepareAddStudent() {
		if (classroomProducer.isAddModeClassroom()) {
			classroomProducer.doSaveForStudents();
		}
		Student student = new Student();
		student.setUserRole(Role.STUDENT);
		this.student = student;
	}

	public void prepareEditStudent(Student student) {
		this.studentStart = entityService.openStart(Student.class, student);
		this.student = student;
	}

	private void setPassword(Student person) {
		try {
			person.setPassWordSalt(EncryptionBean.generateSalt());
			person.setPassWordEncrypted(
					EncryptionBean.getEncryptedPassword(person.getPassWord(), person.getPassWordSalt()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}

	public void doSaveWithoutPassword() {
		entityService.save(student);
	}

	public void doSave() {
		setPassword(student);
		entityService.save(student, classroomProducer.getSelectedClassroom());
	}

	public void addStudentToClassroom(Student student) {
		entityService.addEntityToParent(student, classroomProducer.getSelectedClassroom());
	}

	public void doCancel() {
		entityService.doCancel(Student.class, student, studentStart);
	}

	public void removeStudentFromClassroom(Student student) {
		if (classroomProducer.getSelectedClassroom() != null) {
			classroomProducer.doSave();
		}
		entityService.removeEntityFromParent(student);
		if (classroomProducer.getSelectedClassroom() != null) {
			classroomProducer.refreshSelectedClassroom();
		}

	}

	public void addStudentToClassroom() {
		addStudentToClassroom(this.student);

	}

	public void removeStudentFromClassroom() {
		removeStudentFromClassroom(this.student);

	}
}
