package at.uda.multiplechoice.data;


import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.model.AnswerExam;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.service.EntityService;

@SessionScoped
public class AnswerExamProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;


	private AnswerExam answerExam;

	@Inject
	private QuestionExamProducer questionProducer;
	@Inject
	private EntityService entityService;

	@Produces
	@Named
	public AnswerExam getSelectedAnswerExam() {
		return answerExam;
	}

	public void setSelectedAnswerExam(AnswerExam answer) {
		this.answerExam = answer;
	}

	public void setSelectedAnswerExam(long id) {
		this.answerExam = entityService.find(AnswerExam.class, id);
	}

	@Produces
	@Named
	public boolean isAddModeAnswerExam() {
		return answerExam.getId()==0;
	}

	public void prepareAddAnswer() {
		AnswerExam answer = new AnswerExam();
		this.answerExam = answer;
	}

	public void prepareEditAnswer(AnswerExam answer) {
		this.answerExam = answer;
	}

	public void doSave() {
		entityService.save(answerExam);
	}
	public void doCheck(AnswerExam answer,boolean ok){
		this.answerExam=answer;
		answer.setOkUser(ok);
		entityService.save(answer);
	}

	public void addAnswerToQuestion(AnswerExam answer) {												
		QuestionExam question = questionProducer.getSelectedQuestionExam();
		List<AnswerExam> answersExam = question.getAnswersExam();
		answersExam.size();
		answersExam.add(answer);
		answer.setOrderNumber(answersExam.size());
		if (this.isAddModeAnswerExam()) {
			entityService.save( answer);
		}
		answer.setQuestionExam(question);
		question.setAnswersExam(answersExam);
		entityService.save( question);
	}

}
