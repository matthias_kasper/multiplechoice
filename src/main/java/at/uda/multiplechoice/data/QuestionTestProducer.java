package at.uda.multiplechoice.data;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import at.uda.multiplechoice.model.Question;
import at.uda.multiplechoice.model.QuestionExam;
import at.uda.multiplechoice.model.QuestionTest;
import at.uda.multiplechoice.model.Teacher;
import at.uda.multiplechoice.service.EntityService;
import at.uda.multiplechoice.service.QuestionTestService;

@SessionScoped
@Named
public class QuestionTestProducer implements Serializable {

	private static final long serialVersionUID = -1828660647917534556L;

	private Question question;
	private QuestionTest questionStart = null;

	@Inject
	private TestProducer testProducer;
	@Inject
	private EntityService entityService;
	@Inject
	private QuestionTestService questionService;
	@Inject
	private PersonProducer personProducer;

	@Produces
	@Named
	public QuestionTest getSelectedQuestionTest() {
		return (QuestionTest) question;
	}
	
	@Produces
	@Named
	public QuestionExam getSelectedExamQuestion() {
		return (QuestionExam) question;
	}

	public void setSelectedQuestion(Question question) {
		this.question = question;
	}
	
	public boolean isExamQuestion(){
		return question instanceof QuestionExam;
	}

	@Produces
	@Named
	public boolean isAddModeQuestionTest() {
		return question.getId() == 0;
	}

	public void prepareAddQuestion() {
		if (testProducer.isAddMode()) {
			testProducer.doSaveForQuestion();
		}
		QuestionTest question = new QuestionTest();
		this.question = question;
	}

	public void prepareEditQuestion(QuestionTest question) {
		this.questionStart = entityService.openStart(QuestionTest.class, question);
		this.question = question;

	}

	public void doSave() {
		Teacher teacher = (Teacher) personProducer.getSelectedPerson();
		questionService.doSave((QuestionTest) question, testProducer.getSelectedTest(), teacher);
		testProducer.refreshSelectedTest();
	}

	public void doSaveForAnswers() {
		Teacher teacher = (Teacher) personProducer.getSelectedPerson();
		questionService.doSave((QuestionTest) question, testProducer.getSelectedTest(), teacher);
	}

	public void doSaveOrphan() {
		Teacher teacher = (Teacher) personProducer.getSelectedPerson();
		questionService.doSaveOrphan((QuestionTest) question, teacher);//TEACHER!!
	}

	public void doCancel() {
		entityService.doCancel(QuestionTest.class, (QuestionTest) question, questionStart);
	}

	public QuestionTest getQuestionByTitle(String title) {
		return entityService.getByQueryParam(QuestionTest.class, QuestionTest.findQuestionByTitle, title);
	}

}
