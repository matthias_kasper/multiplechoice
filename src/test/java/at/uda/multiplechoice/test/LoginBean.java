package at.uda.multiplechoice.test;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@SessionScoped
@Named
public class LoginBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String login(String name) {
        return "Hello, " + name;
    }
}
