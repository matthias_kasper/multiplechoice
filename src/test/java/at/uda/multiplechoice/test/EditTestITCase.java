package at.uda.multiplechoice.test;

import java.io.File;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.archive.importer.MavenImporter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class EditTestITCase {

	@Deployment(testable=false)
	public static WebArchive createDeployment() {
	//	return ShrinkWrap.create(JavaArchive.class).addClass(LoginBean.class).addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	//	return ShrinkWrap.create(ZipImporter.class,"target/multiplechoice.war").importFrom(new File("target/multiplechoice.war")).as(WebArchive.class);
	//	return ShrinkWrap.importFrom(new File("target/multiplechoice.war")).as(WebArchive.class);
		//letzte Zeile auskommentieren
        return ShrinkWrap.create(MavenImporter.class).loadPomFromFile("pom.xml").importBuildOutput().as(WebArchive.class);
	}

//	@Inject
//	LoginBean loginBean;

	@Test
	public void login_mary() {
//		String loginResponse = loginBean.login("Mary");
		Assert.assertEquals("q" ,"q");
	}

}