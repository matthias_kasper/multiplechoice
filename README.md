### What is this repository for? ###

* This project is a multiple choice test generator
* Version 0.0.1

* Tested with Wildfly and MySQL.

* Maven is used as build tool.

* Eclipse as Development Platform.

* There is a GUI with several JFS-Frameworks, JQuery and Bootstrap libs.